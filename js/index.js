var template_can = //'<li class="{{status}}">'
                        '<div class="can-data flex-column">'
                            + '<h3>{{product}}</h3>'
                            + '<label>Item ID:</label>'
                            + '<div class="value">{{id}}</div>'
                            + '<label>Temperature (Target):</label>'
                            + '<div class="value">{{temperature}}°c ({{target_temperature}}°c)</div>'
                            // + '<label>Humidity:</label>'
                            // + '<div class="value">{{humidity}}%</div>'
                            // + '<label>Time to Target:</label>'
                            // + '<div class="value">{{time_to_target}}</div>'
                            // + '<label>Expiration:</label>'
                            // + '<div class="value">{{expiration}}</div>'
                            + '<label>Product Health:</label>'
                            + '<div class="value">{{health}}</div>'
                        + '</div>'


                        + '<div class="can-label {{rfid_error}}">'
                        + '<span>{{label_text}}</span>'
                        + '<div class="bar-horizontal"></div>'
                        + '</div>';

                   // + '</li>';

var template_alert = '<li class="alert">'
                        // + '<img src="./img/alert/alert_{{type}}.png">'
                        + '<span class="icon alert-{{type}}"></span>'
                        // + '<img src="./img/alert/priority_{{priority}}.png">'
                        + '<span class="priority priority-{{priority}}"></span>'
                        + '<div>'
                            + '<div><strong>{{title}}</strong></div>'
                            + '<small>{{date_time}}</small>'
                        + '</div>'
                    + '</li>';

var template_alert_log = '<tr class="alert-log">'
                        + '<td>{{date_time}}</td>'
                        + '<td>{{title}}</td>'
                        // + '<img src="./img/alert/alert_{{type}}.png">'
                        // + '<td>{{type}}</td>'
                        // + '<img src="./img/alert/priority_{{priority}}.png">'
                        + '<td>{{priority}}</td>'
                        + '<td>{{sensor}}</td>'
                        + '<td>{{value_threshold}}</td>'
                        + '<td>{{detail}}</td>'
                    + '</tr>';


var active_alerts = [];

var start_time = settings.route_arrival_date || new Date();
var data_cans = {};


function UpdateCanHealth (can) {
    //degredation of time
    can.product_health -= .1;

    //degredation of temp
    var temp_diff = Math.abs(can.target_temperature - can.temperature);
    can.product_health -= (.25 * temp_diff);
    can.product_health = Math.max(can.product_health, 0);
}

function UpdateCans (no_health_degredation) {

//  Health degredation per minute

    if (!no_health_degredation) {
        var can_names = Object.keys(data_cans);

        for (var i = 0; i<can_names.length; i++) {
            if (data_cans[can_names[i]].product_health>0) {
                UpdateCanHealth(data_cans[can_names[i]]);

                CalculateTimeToExpire(data_cans[can_names[i]].product_health, data_cans[can_names[i]].temperature, data_cans[can_names[i]].target_temperature);
            }

            if (data_cans[can_names[i]].product_health<=10 && data_cans[can_names[i]].product_health>0 && !data_cans[can_names[i]].flags.near_expiration) {
                data_cans[can_names[i]].flags.near_expiration = true;
                AddAlert({
                    title:"Item "+data_cans[can_names[i]].display_id+" Near Expiration",
                    type: "expired",
                    priority:"Medium",
                    sensor:"Calculated",
                    value_threshold:"Product Health between 1-10%",
                    detail:""
                });
            }

            if (!data_cans[can_names[i]].temperature || !data_cans[can_names[i]].humidity) {
                data_cans[can_names[i]].status = "no-data";
            }

            if (data_cans[can_names[i]].product_health<=0) {
                if (!data_cans[can_names[i]].flags.expired) {
                    AddAlert({
                        title:"Item "+data_cans[can_names[i]].display_id+" Expired",
                        type: "expired",
                        priority:"High",
                        sensor:"Calculated",
                        value_threshold:"Product Health 0%",
                        detail:""
                    });
                }
                data_cans[can_names[i]].product_health = 0;
                if (data_cans[can_names[i]].temperature && data_cans[can_names[i]].humidity) {
                    data_cans[can_names[i]].status = "expired";
                }
                data_cans[can_names[i]].flags.expired = true;
            }

        }
    }

    if (data_cans[current_can_modal]) {
        UpdateCanModal(data_cans[current_can_modal]);
    }


    ClearCans();
    CreateCans();
    UpdatePieChart();
    UpdatePredictive();

    // CheckSensorStatus();
    var current_time = new Date().getTime() - settings.last_sensor_threshold;
    var date_string;

    if (sensor_status.top_weight) {
        if (sensor_status.top_weight > current_time) {
            jQuery("#can-3-1-3").addClass("current");
        }
        if (sensor_status.top_weight > 0) {
            date_string = new Date(sensor_status.top_weight);
            jQuery("#can-3-1-3 .can-data .value:last-of-type").text("Last Entry: " + date_string.toLocaleTimeString() + " " + date_string.toDateString());
        }
        else {
            jQuery("#can-3-1-3 .can-data .value:last-of-type").text("No Data Entry");
        }
    }
    if (sensor_status.top_rfid) {
        if (sensor_status.top_rfid > current_time) {
            jQuery("#can-3-1-2").addClass("current");
        }
        if (sensor_status.top_rfid > 0) {
            var date_string = new Date(sensor_status.top_rfid);
            jQuery("#can-3-1-2 .can-data .value:last-of-type").text("Last Entry: " + date_string.toLocaleTimeString() + " " + date_string.toDateString());
        }
        else {
            jQuery("#can-3-1-2 .can-data .value:last-of-type").text("No Data Entry");
        }
    }
    if (sensor_status.bottom_weight) {
        if (sensor_status.bottom_weight > current_time) {
            jQuery("#can-3-2-3").addClass("current");
        }
        if (sensor_status.bottom_weight > 0) {
            var date_string = new Date(sensor_status.bottom_weight);
            jQuery("#can-3-2-3 .can-data .value:last-of-type").text("Last Entry: " + date_string.toLocaleTimeString() + " " + date_string.toDateString());
        }
        else {
            jQuery("#can-3-2-3 .can-data .value:last-of-type").text("No Data Entry");
        }
    }
    if (sensor_status.bottom_rfid) {
        if (sensor_status.bottom_rfid > current_time) {
            jQuery("#can-3-2-2").addClass("current");
        }
        if (sensor_status.bottom_rfid > 0) {
            var date_string = new Date(sensor_status.bottom_rfid);
            jQuery("#can-3-2-2 .can-data .value:last-of-type").text("Last Entry: " + date_string.toLocaleTimeString() + " " + date_string.toDateString());
        }
        else {
            jQuery("#can-3-2-2 .can-data .value:last-of-type").text("No Data Entry");
        }
    }

    UpdateData();
}

function ClearCans () {
    for (var u = 0; u<settings.container_stats.slides; u++) {
        for (var i = 0; i<settings.container_stats.rows; i++) {
            for (var j = 0; j<settings.container_stats.columns; j++) {
                jQuery("#can-" + (u+1) + "-" + (i+1) + "-" + (j+1)).empty();
            }
        }
    }
}

function CreateCans () {
    for (var u = 0; u<settings.container_stats.slides; u++) {
        for (var i = 0; i<settings.container_stats.rows; i++) {
            for (var j = 0; j<settings.container_stats.columns; j++) {
                jQuery("#can-" + (u+1) + "-" + (i+1) + "-" + (j+1)).append(CreateCan({
                    status: "empty",
                    s:u,
                    r:i,
                    c:j,
                    rfid_error: container_reference["rfid_error_" + u + "_" + i + "_" + j]
                    // dispensed_from: "0"
                }));

                jQuery("#can-" + (u+1) + "-" + (i+1) + "-" + (j+1)).removeClass();
                jQuery("#can-" + (u+1) + "-" + (i+1) + "-" + (j+1)).addClass("empty");
            }
        }
    }

    var can_names = Object.keys(data_cans);

    for (var i = 0; i<can_names.length; i++) {
        var can  = data_cans[can_names[i]];

        jQuery("#can-" + (can.s+1) + "-" + (can.r+1) + "-" + (can.c+1)).empty();
        jQuery("#can-" + (can.s+1) + "-" + (can.r+1) + "-" + (can.c+1)).append(CreateCan(can));
        jQuery("#can-" + (can.s+1) + "-" + (can.r+1) + "-" + (can.c+1)).removeClass();
        jQuery("#can-" + (can.s+1) + "-" + (can.r+1) + "-" + (can.c+1)).addClass(can.status);
        jQuery("#can-" + (can.s+1) + "-" + (can.r+1) + "-" + (can.c+1)).addClass(can.product_health === 0 ? "expired" : "");
        jQuery("#can-" + (can.s+1) + "-" + (can.r+1) + "-" + (can.c+1)).addClass(can.id === current_can_modal ? "can-selected" : "");
        if (dispensed_text["s"+can.s+"_r"+can.r+"_c"+can.c]) {
            jQuery("#can-" + (can.s+1) + "-" + (can.r+1) + "-" + (can.c+1)).addClass("sensor");
        }
        jQuery("#can-" + (can.s) + "-" + (can.r+1) + "-" + (can.c+1)).addClass(" behind");
    }

    UpdateWebApp(data_cans);
}

var current_can_modal;
function CreateCan(can) {
    var dispensed = dispensed_record["s"+can.s+"_r"+can.r+"_c"+can.c]?"Dispensed from this location: "+dispensed_record["s"+can.s+"_r"+can.r+"_c"+can.c]:dispensed_text["s"+can.s+"_r"+can.r+"_c"+can.c];
    var can_node = jQuery(template_can.replace('{{id}}', can.display_id)
                    .replace('{{product}}', can.product)
                    .replace('{{ble}}', can.id)
                    .replace('{{rfid}}', can.rfid)
                    .replace('{{status}}', can.status)
                    .replace('{{temperature}}', can.temperature)
                    .replace('{{target_temperature}}', can.target_temperature)//Math.max(can.target_temperature, container_reference.temperature))
                    .replace('{{humidity}}', can.humidity)
                    .replace('{{health}}', (Math.floor(can.product_health*10) / 10) + "%")
                    .replace('{{time_to_target}}', can.time_to_target)
                    .replace('{{rfid_error}}', can.rfid_error ? "error" : "")
                    .replace('{{label_text}}', dispensed)
                    .replace('{{expiration}}', can.expiration));

    can_node.click(function(){
        if (can.id) {
            current_can_modal = can.id;
            UpdateCanModal(can);
            jQuery("#can-modal").addClass("show");
            SelectCan(can);
            PauseSlider("display-container");
        }
    });
    return can_node;
}

function SelectCan(can) {
    jQuery("#display-container ul ul>li").removeClass("can-selected");
    jQuery("#can-" + (can.s+1) + "-" + (can.r+1) + "-" + (can.c+1)).addClass("can-selected");
}

function UpdateCanModal (can) {
    jQuery("#can-stats-name").text("Product: " + can.product);

    jQuery("#can-stats-id").text(can.display_id);
    jQuery("#can-stats-carrier-id").text(can.id);
    jQuery("#can-stats-expiration").text(can.expiration);
    var time_in_container = can.time_in_container.getTime ? can.time_in_container.getTime() : new Date(can.time_in_container).getTime();
    var time = Math.round((new Date().getTime() - time_in_container) / 1000 / 60) + " minutes";
    jQuery("#can-stats-time").text(time);

    jQuery("#can-stats-temp").text(can.temperature);
    jQuery("#can-stats-target-temp").text(can.target_temperature);
    jQuery("#can-stats-humidity").text(can.humidity);

    jQuery("#can-stats-weight").text(can.weight);

    jQuery("#can-stats-time-to-target-value").text(can.time_to_target);

    jQuery("#can-stats-health").text(Math.floor(can.product_health*10) / 10);

    var health = Math.floor(can.product_health*10) / 10;

    new Chartist.Pie('#can-stats-health-chart', {
      series: [health,100-health]
    }, {
      donut: true,
      donutWidth: 60,
      donutSolid: true,
      // startAngle: 270,
      showLabel: false
    });



    var options = {
        fullWidth: true,
        axisY: {
            onlyInteger: true
        },
        axisX: {
            labelInterpolationFnc: function(value, i) {
                return i*5;
            }
        }
    };

    if ((!can.time_to_target_chart || !can.time_to_target_chart[0]) && can.temperature) {
        can.time_to_target_chart = [parseInt(can.temperature,10)+RandomNumber(5,7), parseInt(can.temperature,10)+RandomNumber(3,5), parseInt(can.temperature,10)+RandomNumber(2,3), parseInt(can.temperature,10)+RandomNumber(1,2), parseInt(can.temperature,10)];
    }

    /* Initialize the chart with the above settings */
    new Chartist.Line('#can-stats-time-to-target', {
        labels: [0,5,3,4,5],//['0', '5', '10', '15', '20', '25'],
        series: [
            {
                data: can.time_to_target_chart || [20,10,5,3,2,1]
            }
        ]
    }, options);

}

function OpenAlertLog () {
    // jQuery.ajax({
    //     method: "GET",
    //     url: "http://" + window.location.hostname + ":3000/allalerts",
    //     headers: {
    //         // Authorization: "Bearer " + user.token
    //     }
    // }).done(function (entries) {
    //     // finished++;
    //     if (entries) {
    //         Handle_allalerts(entries);
    //     }
    //     else {
    //         console.log("error loading allalerts");
    //     }
    //     // return_function();
    // }).fail(function (error) {
    //     // finished++;
    //     console.error(error);
    //     // return_function();
    // });
    jQuery("#alert-log-modal").addClass("show");
}

function CloseAlertLog () {
    jQuery("#alert-log-modal").removeClass("show");
}


function StartLegal () {
    jQuery("#view-legal-button").click(function(){
        jQuery("#legal-modal").addClass("show");
    });
    jQuery("#close-legal-button").click(function(){
        jQuery("#legal-modal").removeClass("show");
    });
    jQuery("#restart-button").click(function(){
        var can_names = Object.keys(data_cans);

        for (var i = 0; i<can_names.length; i++) {
            data_cans[can_names[i]].product_health = RandomNumber(0,100);
        }
        UpdateCans();
    });
    jQuery("#reset-button").click(function(){
        var data_to_use = {
            data_cans: {},
            location_reference: {},
            active_alerts: [],

            transformed_data: {},
            container_reference: {},
            streaming_data: {},
            dispensed_today: 0,
            dispensed_record: {
                s0_r0_c0: RandomNumber(1,100),
                s0_r0_c1: RandomNumber(1,100),
                s0_r0_c2: RandomNumber(1,100),
                s0_r0_c3: RandomNumber(1,100),
                s0_r1_c0: RandomNumber(1,100),
                s0_r1_c1: RandomNumber(1,100),
                s0_r1_c2: RandomNumber(1,100),
                s0_r1_c3: RandomNumber(1,100),
                s1_r0_c0: RandomNumber(1,100),
                s1_r0_c1: RandomNumber(1,100),
                s1_r0_c2: RandomNumber(1,100),
                s1_r0_c3: RandomNumber(1,100),
                s1_r1_c0: RandomNumber(1,100),
                s1_r1_c1: RandomNumber(1,100),
                s1_r1_c2: RandomNumber(1,100),
                s1_r1_c3: RandomNumber(1,100),
                s2_r0_c0: RandomNumber(1,100),
                s2_r0_c1: 0,//"I/O Gateway Temp/Hum Sensors",//RandomNumber(1,100),
                s2_r0_c2: 0,//"I/O Gateway Weight Sensors",//RandomNumber(1,100),
                s2_r0_c3: RandomNumber(1,100),
                s2_r1_c0: RandomNumber(1,100),
                s2_r1_c1: 0,//"I/O Gateway Temp/Hum Sensors",//RandomNumber(1,100),
                s2_r1_c2: 0,//"I/O Gateway Weight Sensors",//RandomNumber(1,100),
                s2_r1_c3: RandomNumber(1,100)
            },
            total_products: 0
        };
        jQuery.ajax({
            method: "POST",
            url: "http://" + window.location.hostname + ":3000/post",
            // contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(data_to_use),
            headers: {
                // Authorization: "Bearer " + user.token
                "Content-Type": 'application/json'
            }
        }).done(function (entries) {
            console.log("DATA RESET");
        }).fail(function (error) {
            if (error.status === 200) {
                console.log("DATA RESET");
                GetGlobalData();
            } else {
                console.log("reset failed");
            }
        });
    });
    jQuery("#view-about-button").click(function(){
        jQuery("#about-modal").addClass("show");
    });
    jQuery("#close-about-button").click(function(){
        jQuery("#about-modal").removeClass("show");
    });
    jQuery("#close-can-button").click(function(){
        jQuery("#can-modal").removeClass("show");
        SelectCan({});
        current_can_modal = null;
    });
}

var last_total_alerts = 0;
function StartAlerts () {

    // jQuery("#alerts-shown").text(0);
    // jQuery("#alerts-total").text(0);

    jQuery("#view-alert-log-button").click(function(){
        OpenAlertLog();
    });
    jQuery("#close-alert-log-button").click(function(){
        CloseAlertLog();
    });


    setInterval(function () {
        alert_history_chart_data.push(active_alerts.length - last_total_alerts);
        last_total_alerts = active_alerts.length;
        UpdateStreamingChart();
    }, settings.alert_history_interval * 60 * 1000);
    // AddAlert();
}

function AddAlert(data) {

    var new_alert = {
        title: data.title,
        type: data.type,
        priority: data.priority,
        sensor: data.sensor,
        itemId: data.id,
        value_threshold: data.value_threshold,
        detail: data.detail || ""
    };

    var date_string = new Date().toString();
    new_alert.date_time = date_string.substring(0, date_string.indexOf("GMT"));

    active_alerts.push(new_alert);

    UpdateAlerts();
}


function UpdateAlerts() {

    var display_number = Math.min(active_alerts.length, 4);

    jQuery("#alerts-shown").text(display_number);
    jQuery("#alerts-total").text(active_alerts.length);


    ClearAlerts();
    CreateAlerts(display_number);
    ClearAlertsLog();
    CreateAlertsLog(display_number);

}

function ClearAlerts() {
    jQuery("#alerts").empty();
}

function CreateAlerts (display_number) {
    for (var i = 0; i<display_number; i++) {
        jQuery("#alerts").append(CreateAlert(active_alerts[active_alerts.length - i - 1]));
    }
}

function ClearAlertsLog() {
    jQuery("#alerts-log").empty();
}

function CreateAlertsLog () {
    for (var i = 0; i<active_alerts.length; i++) {
        jQuery("#alerts-log").prepend(CreateAlertLog(active_alerts[i]));
    }
}

function CreateAlert(alert) {
    var node_string = template_alert.replace('{{title}}', alert.title)
                    .replace('{{date_time}}', alert.date_time)
                    .replace('{{type}}', alert.type)
                    .replace('{{priority}}', alert.priority)
                    .replace('{{id}}', alert.id);

        node_string = node_string.replace('[ItemId]', alert.itemId);
        node_string = node_string.replace('[User]', alert.itemId);

    var alert_node = jQuery(node_string);

    return alert_node;
}
function CreateAlertLog(alert) {
    var node_string = template_alert_log.replace('{{title}}', alert.title)
                    .replace('{{date_time}}', alert.date_time)
                    .replace('{{type}}', alert.type)
                    .replace('{{priority}}', alert.priority)
                    .replace('{{sensor}}', alert.sensor)
                    .replace('{{value_threshold}}', alert.value_threshold)
                    .replace('{{detail}}', alert.detail)
                    .replace('{{id}}', alert.id);

    node_string = node_string.replace('[ItemId]', RandomNumber(1,999));
    node_string = node_string.replace('[User]', RandomNumber(1,999));

    var alert_node = jQuery(node_string);

    return alert_node;
}



function CustomPieChart (options) {
  return function ctPointLabels(chart) {
    var defaultOptions = {
        sliceMargin: 4,
        labelOffset: {
            x: 0,
            y: -15
        },
        labelClass: "label-percent"
    };

    options = Chartist.extend({}, defaultOptions, options);

    if(chart instanceof Chartist.Pie) {
        var percent = 0;
        chart.on('draw', function(data) {

            if (data.type === 'slice') {
                // data.radius *= Math.random();
                percent = Math.round((data.value / data.totalDataSum) * 1000) / 10;


                var d = data.element.attr("d");
                var new_d = "";

                var arcSweep = data.endAngle - data.startAngle <= 180 ? '0' : '1';

                var r = data.radius;
                var new_r = r - Math.max(0,Math.round((1 - (data.value/(data.totalDataSum / 2))) * (r / 3)));


                start = Chartist.polarToCartesian(data.center.x, data.center.y, new_r, data.startAngle);
                end = Chartist.polarToCartesian(data.center.x, data.center.y, new_r, data.endAngle);


                array = [
                    // Start at the end point from the cartesian coordinates
                    'M', end.x, end.y,
                    // Draw arc
                    'A', new_r, new_r, 0, arcSweep, 0, start.x, start.y,
                    'L', data.center.x, data.center.y,
                    'Z'
                ];


                new_d = array.join(" ");

                data.element.attr({"d": new_d});
                data.element.attr({"style": "filter:url(#dropshadow)"});

            }

            if(data.type === 'label') {
                var half_x = data.group.parent()._node.clientWidth / 2;
                var w = data.group.parent()._node.clientWidth;
                var h = data.group.parent()._node.clientHeight;
                var half_y = data.group.parent()._node.clientHeight / 2;
                var length = 70;
                var line_padding = 30;
                var circle_radius = 3;

                var line = {
                    startx: (data.x + (line_padding * ((half_x - data.x) / w))),
                    starty: data.y,
                    midx: (length * ((half_x - data.x) / w)),
                    midy: 0,
                    endx: (length * ((half_x - data.x) / w)),
                    endy: (length * ((half_y - data.y) / h))
                };

                if (percent > 0) {

                    data.element.attr({"opacity": 1});
                    data.group.elem('path', {
                        d: "m " + line.startx
                          + " " + line.starty
                        + " l " + line.midx
                          + " " + line.midy
                        + " l " + line.endx
                          + " " + line.endy
                    }, "label-line");

                    data.group.elem('circle', {
                        cx: line.startx+line.midx+line.endx+((circle_radius * 2.5) * ((half_x - data.x) / w)),
                        cy: line.starty+line.midy+line.endy+((circle_radius * 2.5) * ((half_y - data.y) / h)),
                        r: circle_radius
                        //style: "line"//'text-anchor: ' + options.textAncho
                    }, "label-line");


                    data.group.elem('text', {
                        x: data.x + options.labelOffset.x,
                        y: data.y + options.labelOffset.y / 2,
                        "text-anchor": data.element.attr("text-anchor")
                    }, options.labelClass).text(percent + "%");

                } else {

                    data.element.attr({"opacity": 0});
                }

                data.element.attr({"dy": data.y - (options.labelOffset.y / 2)});
            }
        });
    }
  }
}


function CreatePieChart (data) {

    var options = {
        labelInterpolationFnc: function(value) {
            return value[0]
        },
        plugins: [
            CustomPieChart({})//,
            // Chartist.plugins.ctSliceDonutMargin({
            //     sliceMargin: 6
            // })
        ]
    };

    var responsiveOptions = [
      ['screen and (min-width: 640px)', {
        chartPadding: 10,
        labelOffset: 10,
        labelDirection: 'explode',
        labelInterpolationFnc: function(value) {
          return value;
        }
      }],
      ['screen and (min-width: 1024px)', {
        labelOffset: 30,
        chartPadding: 30
      }],
      ['screen and (min-width: 1524px)', {
        labelOffset: 50,
        chartPadding: 50
      }]
    ];

    new Chartist.Pie('#pie-chart', data, options, responsiveOptions);
}

function CreateProductPieChart (data) {

    var options = {
        labelInterpolationFnc: function(value) {
            return value[0]
        },
        plugins: [
            CustomPieChart({})//,
            // Chartist.plugins.ctSliceDonutMargin({
            //     sliceMargin: 6
            // })
        ]
    };

    var responsiveOptions = [
      ['screen and (min-width: 640px)', {
        chartPadding: 10,
        labelOffset: 10,
        labelDirection: 'explode',
        labelInterpolationFnc: function(value) {
          return value;
        }
      }],
      ['screen and (min-width: 1024px)', {
        labelOffset: 30,
        chartPadding: 30
      }],
      ['screen and (min-width: 1524px)', {
        labelOffset: 50,
        chartPadding: 50
      }]
    ];

    new Chartist.Pie('#pie-chart-products', data, options, responsiveOptions);
}

function UpdatePieChart() {
    var labels = [];
    var series = [];

    var total_cans = 0;
    var can_types = {};

    var can_names = Object.keys(data_cans);

    for (var i = 0; i<can_names.length; i++) {
        var can = data_cans[can_names[i]];

        if (can.status != "empty" && !can.removed) {
            if (can_types[can.status]) {
                can_types[can.status]++;
            } else {
                can_types[can.status] = 1;
            }
            total_cans ++;
        }

    }


    for (var i = 0; i<data_inventory.pie_chart.length; i++) {
        labels.push(data_inventory.pie_chart[i].title);

        series.push(can_types[data_inventory.pie_chart[i].code] || 0);
    }


    jQuery("#inventory-total").text(total_cans);
    jQuery("#inventory-ready").text(can_types.ready || 0);
    jQuery("#items-ready-soon").text((can_types["ready-soon"] || 0) + (can_types["ready-soon"] === 1 ? " Item" : " Items"));
    jQuery("#items-expired").text((can_types.expired || 0) + (can_types.expired === 1 ? " Item" : " Items"));

    CreatePieChart({
        labels: labels,
        series: series
    });


    var product_labels = [];
    var product_series = [];

    var can_products = {};

    for (var i = 0; i<can_names.length; i++) {
        var can = data_cans[can_names[i]];

        if (can.status != "empty" && !can.removed) {
            if (can_products[can.product]) {
                can_products[can.product]++;
            } else {
                can_products[can.product] = 1;
            }
        }

    }

    var can_products_names = Object.keys(can_products);

    for (var i = 0; i<can_products_names.length; i++) {
        product_labels.push(can_products_names[i]);

        product_series.push(can_products[can_products_names[i]] || 0);
    }


    CreateProductPieChart({
        labels: product_labels,
        series: product_series
    });
}

function UpdateInventory () {

    var dispensed_names = Object.keys(dispensed_record);
    var dispensed_total = 0;

    for (var d = 0; d<dispensed_names.length; d++) {
        if (dispensed_record[dispensed_names[d]] || dispensed_record[dispensed_names[d]] === 0) {
            dispensed_total += dispensed_record[dispensed_names[d]];
        }
    }

    jQuery("#inventory-dispensed-total").text(dispensed_total);//dispensed_names.length);

    // jQuery("#inventory-dispensed-total").text(data_inventory.dispensed_total);
    jQuery("#inventory-dispensed-today").text(dispensed_today);
    UpdatePieChart();
}

function UpdateContainer () {
    // jQuery("#container-temp").text(data_container.container_temp);
    // jQuery("#container-humidity").text(data_container.container_humidity);
    // jQuery("#environment-temp").text(data_container.environment_temp);
    // jQuery("#environment-humidity").text(data_container.environment_humidity);
}

function StartTimeAtLocation (route) {
    UpdateTimeAtLocation(route);
    setInterval(function () {
        UpdateTimeAtLocation(route);
    }, 60000);
}

function UpdateTimeAtLocation (route) {
    var time = new Date() - start_time;
    var hours = Math.floor(time / 1000 / 60 / 60);
    var minutes = Math.floor(((time / 1000 / 60 / 60) - hours) * 60);
    jQuery("#time-at-location").text(
        hours + " hours, " +
        minutes + " minutes"
    );
}

function UpdateGeospatial (route) {
    jQuery("#travel-time").text(data_geospatial[route].travel_time_to_location);
    jQuery("#travel-distance").text(data_geospatial[route].travel_distance_to_location);
    jQuery("#travel-distance-km").text(data_geospatial[route].travel_distance_to_location_km);


    jQuery("#to-address").text(data_geospatial[route].to.address);
    jQuery("#to-temperature").text(data_geospatial[route].to.temp);
    jQuery("#to-latitude").text(data_geospatial[route].to.latitude);
    jQuery("#to-longitude").text(data_geospatial[route].to.longitude);

    jQuery("#to-zip-code").text(data_geospatial[route].to.zip_code);
    jQuery("#to-city").text(data_geospatial[route].to.city);
    jQuery("#to-county").text(data_geospatial[route].to.county);
    jQuery("#to-population-density").text(data_geospatial[route].to.population_density);
    jQuery("#to-cost-of-living-index").text(data_geospatial[route].to.cost_of_living_index);
    jQuery("#to-median-household-size").text(data_geospatial[route].to.median_household_size);
    jQuery("#to-median-household-income").text(data_geospatial[route].to.median_household_income);

    jQuery("#from-address").text(data_geospatial[route].from.address);
    jQuery("#from-temperature").text(data_geospatial[route].from.temp);
    jQuery("#from-latitude").text(data_geospatial[route].from.latitude);
    jQuery("#from-longitude").text(data_geospatial[route].from.longitude);

}

function CreateLineChart (data) {

    /* Set some base options (settings will override the default settings in Chartist.js *see default settings*). We are adding a basic label interpolation function for the xAxis labels. */
    var options = {
        axisY: {
            onlyInteger: true
        },
        axisX: {
            labelInterpolationFnc: function(value, i) {
                if (alert_history_chart_data.length >= 10) {
                    return (alert_history_chart_data.length - (10-i)) * settings.alert_history_interval;
                }
                return settings.alert_history_interval*i;
            }
        }
    };

    /* Now we can specify multiple responsive settings that will override the base settings based on order and if the media queries match. In this example we are changing the visibility of dots and lines as well as use different label interpolations for space reasons. */
    var responsiveOptions = [

    ];

    /* Initialize the chart with the above settings */
    new Chartist.Line('#alert-chart', data, options, responsiveOptions);
}

function GetLast10 (data) {
    return data.slice(-10);
}
var alert_history_chart_data = [0];
function UpdateStreamingChart() {
    CreateLineChart({
        labels: [],//['0', '5', '10', '15', '20', '25'],
        series: [
            {
                data: GetLast10(alert_history_chart_data)//[1, 2, 3, 5, 8, 13]
            }
        ]
    });
}
function UpdateStreaming (activity) {
    UpdateStreamingChart();

    jQuery.ajax({
        method: "GET",
        url: "http://" + window.location.hostname + ":3000/perhour",
        headers: {
            // Authorization: "Bearer " + user.token
        }
    }).done(function (entries) {

        // callback(entries.results);
        jQuery("#data-points-minute").text(Math.round(entries.results[0].ACTIVITY_PERHOUR / 60));

    }).fail(function (error) {
        console.error(error);
        // jQuery("#error-modal").addClass("show");
        // jQuery("#loading-modal").removeClass("show");
    });

    jQuery.ajax({
        method: "GET",
        url: "http://" + window.location.hostname + ":3000/abnormal",
        headers: {
            // Authorization: "Bearer " + user.token
        }
    }).done(function (entries) {

        // callback(entries.results);
        jQuery("#abnormalities").text(Math.round(entries.results[0].ACTIVITY_ABRORMALITIESPERHOUR / 60));

    }).fail(function (error) {
        console.error(error);
        // jQuery("#error-modal").addClass("show");
        // jQuery("#loading-modal").removeClass("show");
    });

    jQuery("#data-sources").text(activity.length);
    jQuery("#under-temp").text(streaming_data.under_temp);
    jQuery("#over-temp").text(streaming_data.over_temp);


}

function UpdatePredictive () {
    var can_names = Object.keys(data_cans);

    var ready_hour = 0;
    var ready_2hour = 0;
    var ready_day = 0;

    var total_ready_time = 0;
    var average_time;
    var total_cans = 0;

    var expire_hour = 0;
    var expire_2hour = 0;
    var expire_day = 0;

    for (var i = 0; i<can_names.length; i++) {
        var can  = data_cans[can_names[i]];

        if (can.time_to_target_number < 60) {
            ready_hour++;
        }
        else if (can.time_to_target_number < 120) {
            ready_2hour++;
        }
        else if (can.time_to_target_number < 1440) {
            ready_day++;
        }

        if (can.time_to_target_number) {
            total_ready_time += can.time_to_target_number;
            total_cans++;
        }

        if (can.time_to_expire < 1) {
            expire_hour++;
        }
        else if (can.time_to_expire < 2) {
            expire_2hour++;
        }
        else if (can.time_to_expire < 24) {
            expire_day++;
        }
    }

    average_time = Math.round(total_ready_time / total_cans);

    jQuery("#items-ready-hour").text(ready_hour + (ready_hour === 1 ? " item" : " items"));
    jQuery("#items-ready-2hour").text(ready_2hour + (ready_2hour === 1 ? " item" : " items"));
    jQuery("#items-ready-day").text(ready_day + (ready_day === 1 ? " item" : " items"));

    jQuery("#items-ready-average").text(CalculateTimeToTemp(22) + " minutes");//average_time + (average_time === 1 ? " minute" : " minutes"));

    jQuery("#items-expire-hour").text(expire_hour + (expire_hour === 1 ? " item" : " items"));
    jQuery("#items-expire-2hour").text(expire_2hour + (expire_2hour === 1 ? " item" : " items"));
    jQuery("#items-expire-day").text(expire_day + (expire_day === 1 ? " item" : " items"));
}

function NextSlide (slider) {
    GotoSlide(slider, slider.current_slide+1);
}

function GotoSlide (slider, slide) {

    if (sliders[slider.id].interval && settings[slider.id+"_delay"]) {
        clearInterval(sliders[slider.id].interval);
        startInterval(slider.id);
    }

    jQuery("#" + slider.id + ">article").removeClass("show");

    var slide_node_string = "#" + slider.id + ">article:nth-of-type(" + slide + ")";
    var slide_node = jQuery(slide_node_string);
    if (slide_node.length == 0) {
        slide = 1;
        slide_node_string = "#" + slider.id + ">article:nth-of-type(1)";
    }
    jQuery(slide_node_string).addClass("show");
    slider.current_slide = slide;


    jQuery("#" + slider.id + "-position>*").removeClass("selected");
    jQuery("#" + slider.id + "-position>*:nth-of-type(" + slide + ")").addClass("selected");
    jQuery(slide_node_string + " .sub-slider").each(function() {
        GotoSlide(sliders[this.id], 1);
    });

    if (jQuery("video", slide_node_string).length > 0) {
        jQuery("video", slide_node_string)[0].currentTime = 0;
        jQuery("video", slide_node_string)[0].play();
    }

    if (slider.id === "slider-info" && !settings.use_backup_videos) {
        console.log(slide);
        if (slide === 2) {
            console.log("---geo---");
            if (pin_animation_data) {
                StartPin(pin_animation_data);
            } else {
                start_pin = true;
            }
        } else {
            start_pin = false;
            if (map) {
                map.entities.clear();
            }
            clearInterval(pin_interval);
        }
    }
}

function startInterval (id) {
    sliders[id].interval = setInterval(function() {
        NextSlide(sliders[id]);
    }, settings[id+"_delay"] || 5000);
}

function PauseSlider(id) {
    clearInterval(sliders[id].interval);
    sliders[id].interval = null;
    jQuery("#" + id + "-pause").addClass("selected");
    jQuery("#" + id + "-play").removeClass("selected");
}


var sliders = {};
function StartSliders () {
    jQuery(".slider, .sub-slider").each(function(){
        var _this = this;
        sliders[this.id] = {
            interval: null,
            current_slide: 1,
            id: this.id
        }
        if (settings[this.id+"_delay"]) {
            startInterval(this.id);
        }
        jQuery("#" + this.id + "-play").addClass("selected");

        jQuery("#" + this.id + "-pause").click(function() {
            PauseSlider(_this.id);
        });
        jQuery("#" + this.id + "-play").click(function() {
            if (settings[_this.id+"_delay"]) {
                startInterval(_this.id);
            }
            jQuery(this).addClass("selected");
            jQuery("#" + _this.id + "-pause").removeClass("selected");
        });
        jQuery("#" + this.id + "-next").click(function() {
            NextSlide(sliders[_this.id]);
        });

        jQuery("#" + this.id + "-position>*").removeClass("selected");
        jQuery("#" + this.id + "-position>*:nth-of-type(1)").addClass("selected");

        jQuery("#" + this.id + "-position>*").each(function(u,i) {
            jQuery(this).click(function() {
                GotoSlide(sliders[_this.id], u+1);
            });
        });

        GotoSlide(sliders[_this.id], 1);
    });
}

function StartVideoControls() {

    // Video
    var video = document.getElementById("video");

    // Buttons
    var playButton = document.getElementById("play-pause");
    var muteButton = document.getElementById("mute");
    var fullScreenButton = document.getElementById("full-screen");

      // Sliders
    var seekBar = document.getElementById("seek-bar");
    var volumeBar = document.getElementById("volume-bar");
    // Event listener for the seek bar
    function seek_change() {
      // Calculate the new time
        var time = video.duration * (seekBar.value / 100);

      // Update the video time
        video.currentTime = time;
    }
    seekBar.addEventListener("change", seek_change);
    seekBar.addEventListener("input", seek_change);

    // Update the seek bar as the video plays
    video.addEventListener("timeupdate", function() {
      // Calculate the slider value
        var value = (100 / video.duration) * video.currentTime;

      // Update the slider value
        seekBar.value = value;
    });

    // Pause the video when the slider handle is being dragged
    seekBar.addEventListener("mousedown", function() {
        video.pause();
    });

    // Play the video when the slider handle is dropped
    seekBar.addEventListener("mouseup", function() {
        video.play();
    });

    // Play the video when the slider handle is dropped
    video.addEventListener("click", function() {
        if (this.paused == false) {
            this.pause();
        } else {
            this.play();
        }
    });
}
function LoadVideo(source) {
    document.getElementById("mp4_src").src = source;
    document.getElementById("video").load();
}


function HandleAuthorizedDoor () {
    jQuery("#door-status").text("Unlocked for 20 Seconds");
    jQuery("#door-status-top").text("Unlocked for 20 Seconds");
    jQuery("#locked-icon").addClass("hide");
    jQuery("#unlocked-icon").removeClass("hide");
    jQuery("#authorized-icon").addClass("hide");
    jQuery("#unauthorized-icon").addClass("hide");

    setTimeout(function() {
        if (!container_reference.opened) {
            HandleCloseDoor();
        }
    }, settings.door_time);
}

var door_image_refresh;

function HandleOpenDoor () {
    jQuery("#door-status").text("Opened with authorization");
    jQuery("#door-status-top").text("Opened with authorization");
    jQuery("#locked-icon").addClass("hide");
    jQuery("#unlocked-icon").addClass("hide");
    jQuery("#authorized-icon").removeClass("hide");
    jQuery("#unauthorized-icon").addClass("hide");

    if (sliders["slider-info"]) {
        GotoSlide(sliders["slider-info"],0);
    }

    jQuery("#can-modal").removeClass("show");
    SelectCan({});
    current_can_modal = null;
    jQuery("#door-image").addClass("show");
    jQuery("#pie-slider").addClass("hide");
    jQuery('#door-img').attr('src',"http://" + window.location.hostname + ':3000/img/camimage.jpg?rando='+Math.random());

    door_image_refresh = setInterval(function () {
        jQuery('#door-img').attr('src',"http://" + window.location.hostname + ':3000/img/camimage.jpg?rando='+Math.random());
    }, settings.door_image_refresh_rate);
}

function HandleUnauthorizedDoor () {
    jQuery("#door-status").text("Opened without authorization");
    jQuery("#door-status-top").text("Opened without authorization");
    jQuery("#locked-icon").addClass("hide");
    jQuery("#unlocked-icon").addClass("hide");
    jQuery("#authorized-icon").addClass("hide");
    jQuery("#unauthorized-icon").removeClass("hide");

    if (sliders["slider-info"]) {
        GotoSlide(sliders["slider-info"],0);
    }

    jQuery("#can-modal").removeClass("show");
    SelectCan({});
    current_can_modal = null;
    jQuery("#door-image").addClass("show");
    jQuery("#pie-slider").addClass("hide");
    jQuery('#door-img').attr('src',"http://" + window.location.hostname + ':3000/img/camimage.jpg?rando='+Math.random());

    door_image_refresh = setInterval(function () {
        jQuery('#door-img').attr('src',"http://" + window.location.hostname + ':3000/img/camimage.jpg?rando='+Math.random());
    }, 2000);
}

function HandleCloseDoor () {
    jQuery("#door-status").text("Locked");
    jQuery("#door-status-top").text("Locked");
    jQuery("#locked-icon").removeClass("hide");
    jQuery("#unlocked-icon").addClass("hide");
    jQuery("#authorized-icon").addClass("hide");
    jQuery("#unauthorized-icon").addClass("hide");
    container_reference.authorized = false;

    jQuery("#door-image").removeClass("show");
    jQuery("#pie-slider").removeClass("hide");
    window.clearInterval(door_image_refresh);
    door_image_refresh = null;
}




var transformed_data = {};
var location_reference = {};
var container_reference = {};
var streaming_data = {
    under_temp: 0,
    over_temp: 0
};
var dispensed_today = 0;
var dispensed_record = {
    s0_r0_c0: RandomNumber(1,100),
    s0_r0_c1: RandomNumber(1,100),
    s0_r0_c2: RandomNumber(1,100),
    s0_r0_c3: RandomNumber(1,100),
    s0_r1_c0: RandomNumber(1,100),
    s0_r1_c1: RandomNumber(1,100),
    s0_r1_c2: RandomNumber(1,100),
    s0_r1_c3: RandomNumber(1,100),
    s1_r0_c0: RandomNumber(1,100),
    s1_r0_c1: RandomNumber(1,100),
    s1_r0_c2: RandomNumber(1,100),
    s1_r0_c3: RandomNumber(1,100),
    s1_r1_c0: RandomNumber(1,100),
    s1_r1_c1: RandomNumber(1,100),
    s1_r1_c2: RandomNumber(1,100),
    s1_r1_c3: RandomNumber(1,100),
    s2_r0_c0: RandomNumber(1,100),
    s2_r0_c1: 0,//"I/O Gateway Temp/Hum Sensors",//RandomNumber(1,100),
    s2_r0_c2: 0,//"I/O Gateway Weight Sensors",//RandomNumber(1,100),
    s2_r0_c3: RandomNumber(1,100),
    s2_r1_c0: RandomNumber(1,100),
    s2_r1_c1: 0,//"I/O Gateway Temp/Hum Sensors",//RandomNumber(1,100),
    s2_r1_c2: 0,//"I/O Gateway Weight Sensors",//RandomNumber(1,100),
    s2_r1_c3: RandomNumber(1,100)
};
var dispensed_text = {
    s2_r0_c1: "I/O Gateway RFID Sensors",//RandomNumber(1,100),
    s2_r0_c2: "I/O Gateway Weight Sensors",
    s2_r1_c1: "I/O Gateway RFID Sensors",//RandomNumber(1,100),
    s2_r1_c2: "I/O Gateway Weight Sensors"
}
var total_products = 1;

function HandleProdPlacement(item) {
    if (item.EVENT_DESCRIPTION === "RFID Error") {
        if (rfid_sensor_positions[item.MACHINEID]) {

            var s = rfid_sensor_positions[item.MACHINEID].s;
            var r = rfid_sensor_positions[item.MACHINEID].r;
            var c = rfid_sensor_positions[item.MACHINEID].c;

            if (!container_reference["rfid_error_" + s + "_" + r + "_" + c]) {
                AddAlert({
                    title:"RFID Communication Error: " + item.MACHINEID,
                    id: location_reference[item.MACHINEID],
                    type: "network",
                    priority: "High",
                    sensor: item.MACHINEID,
                    value_threshold:"RFID Communication Error",
                    detail: ""
                });

                //jQuery("#can-" + (s+1) + "-" + (r+1) + "-" + (c+1) + ">.can-label").addClass("error");
                container_reference["rfid_error_" + s + "_" + r + "_" + c] = true;
                if (data_cans[location_reference[item.MACHINEID]]) {
                    data_cans[location_reference[item.MACHINEID]].rfid_error = true;
                }
            }
        }
    } else if (item.EVENT_DESCRIPTION === "RFID Found") {
        if (rfid_sensor_positions[item.MACHINEID]) {
            var rfid = sensor_rfid_values[item.EVENT_VALUE];
            var s = rfid_sensor_positions[item.MACHINEID].s;
            var r = rfid_sensor_positions[item.MACHINEID].r;
            var c = rfid_sensor_positions[item.MACHINEID].c;

            container_reference["rfid_error_" + s + "_" + r + "_" + c] = false;
            if (data_cans[location_reference[item.MACHINEID]]) {
                data_cans[location_reference[item.MACHINEID]].rfid_error = false;
            }

            BuildCanObject({
                id: sensor_rfid_values[item.EVENT_VALUE] || item.EVENT_VALUE,//,
                ble_id: sensor_rfid_values[item.EVENT_VALUE],
                location_id: item.MACHINEID,
                EVENT_TIME: item.EVENT_TIME,
                target_temperature: settings.target_temperature,
                temperature: transformed_data[rfid+"T"] ? transformed_data[rfid+"T"].EVENT_VALUE : null,
                humidity: transformed_data[rfid+"H"] ? transformed_data[rfid+"H"].EVENT_VALUE : null,
                product_health: RandomNumber(0,100),
                rfid: item.EVENT_VALUE
            },s,r,c);
        } else {
            console.error("Could not find a can location for: " + item.MACHINEID);
        }
    } else if (item.EVENT_DESCRIPTION === "RFID Not Found" && data_cans[location_reference[item.MACHINEID]]) {

        var s = data_cans[location_reference[item.MACHINEID]].s;
        var r = data_cans[location_reference[item.MACHINEID]].r;
        var c = data_cans[location_reference[item.MACHINEID]].c;


        dispensed_record[
            "s" + s +
            "_r" + r +
            "_c" + c
        ]++;
        dispensed_today++;

        container_reference["rfid_error_" + s + "_" + r + "_" + c] = false;
            if (data_cans[location_reference[item.MACHINEID]]) {
                data_cans[location_reference[item.MACHINEID]].rfid_error = false;
            }

        data_cans[location_reference[item.MACHINEID]].s = -1;
        data_cans[location_reference[item.MACHINEID]].r = -1;
        data_cans[location_reference[item.MACHINEID]].c = -1;
        data_cans[location_reference[item.MACHINEID]].removed = new Date().getTime();

        UpdateInventory();

        if (current_can_modal === location_reference[item.MACHINEID]) {
            jQuery("#can-modal").removeClass("show");
            SelectCan({});
            current_can_modal = null;
        }

        AddAlert({
            title:"Item " + data_cans[location_reference[item.MACHINEID]].display_id + " Removed",
            id: location_reference[item.MACHINEID],
            type: "removed",
            priority: "Medium",
            sensor: item.MACHINEID,
            value_threshold:"Item removed from inventory",
            detail: ""
        });

        var _item = item;
        var display_id = data_cans[location_reference[item.MACHINEID]].display_id;

        setTimeout(function () {
            if (!data_cans[location_reference[_item.MACHINEID]] || data_cans[location_reference[_item.MACHINEID]].removed) {
                AddAlert({
                    title:"Item " + display_id + " Dispensed",
                    id: location_reference[item.MACHINEID],
                    type: "removed",
                    priority: "Medium",
                    sensor: item.MACHINEID,
                    value_threshold:"Item removed from inventory",
                    detail: ""
                });
            }

        }, settings.can_removal_threshold);

        location_reference[item.MACHINEID] = null;
    }
}

function CalculateWeight (weight) {
    var chart = {
        "29": 9.5,
        "34": 9.2,
        "39": 9,
        "44": 8.6,
        "49": 8.5,
        "59": 8.4,
        "74": 8,
        "99": 7,
        "149": 6,
        "199": 4,
        "299": 3,
        "399": 2.3,
        "499": 2.2,
        "699": 2.1,
        "799": 2,
        "999": 1.9,
        "1299": 1.8,
        "1999": 1.5,
        "2999": 1.2,
        "4999": 1,
        "7999": 0.7,
        "11999": 0.6,
        "14999": 0.5,
        "19999": 0.4,
        "24999": 0.2,
        "30001": 0
    };
    var chart_names = [
        29,
        34,
        39,
        44,
        49,
        59,
        74,
        99,
        149,
        199,
        299,
        399,
        499,
        699,
        799,
        999,
        1299,
        1999,
        2999,
        4999,
        7999,
        11999,
        14999,
        19999,
        24999,
        30001
    ];

    var value = 0;
    for (var i=0; i<chart_names.length; i++) {
        value = chart_names[i];
        if (weight < value) {
            return chart[chart_names[i] + ""];
        }
    }
    return 0;
}

function CalculateTimeToTemp (temp) {
    var fridge = settings.default_fridge_temp;//container_reference.temperature
    return Math.round(Math.log((settings.target_temperature - fridge) / (temp - fridge)) / settings.temp_k);
}

function CalculateTimeToExpire (health, temperature, target_temperature) {
    var temp_diff = Math.abs(target_temperature - temperature);

    return (health / ((.25 * temp_diff) + .1));
}

var sensor_status = {};

function CheckSensorStatus () {
    //11-14 19-22 27-28
    var top = [11,12,13,14,19,20,21,22,27,28];
    //15-18 23-26 29-30
    var bottom = [15,16,18,23,24,25,26,29,30];

    // jQuery("#can-3-1-3").removeClass("current");
    // jQuery("#can-3-2-3").removeClass("current");

    // jQuery("#can-3-1-2").removeClass("current");
    // jQuery("#can-3-2-2").removeClass("current");

    sensor_status = {};

    for (var t=0; t<top.length; t++) {
        if (transformed_data["FSR"+top[t]] && new Date(transformed_data["FSR"+top[t]].EVENT_TIME).getTime()) {
            console.log("current top weight is good");
            // jQuery("#can-3-1-3").addClass("current");

            sensor_status.top_weight = Math.max(new Date(transformed_data["FSR"+top[t]].EVENT_TIME).getTime(), (sensor_status.top_weight || 0));
        }
        if (transformed_data["RFID"+top[t]] && new Date(transformed_data["RFID"+top[t]].EVENT_TIME).getTime()) {
            console.log("current top rfid is good");
            // jQuery("#can-3-1-2").addClass("current");
            sensor_status.top_rfid = Math.max(new Date(transformed_data["RFID"+top[t]].EVENT_TIME).getTime(), (sensor_status.top_rfid || 0));
        }
    }
    for (var b=0; b<bottom.length; b++) {
        if (transformed_data["FSR"+bottom[b]] && new Date(transformed_data["FSR"+bottom[b]].EVENT_TIME).getTime()) {
            console.log("current bottom weight is good");
            // jQuery("#can-3-2-3").addClass("current");
            sensor_status.bottom_weight = Math.max(new Date(transformed_data["FSR"+bottom[b]].EVENT_TIME).getTime(), (sensor_status.bottom_weight || 0));
        }
        if (transformed_data["RFID"+bottom[b]] && new Date(transformed_data["RFID"+bottom[b]].EVENT_TIME).getTime()) {
            console.log("current bottom rfid is good");
            // jQuery("#can-3-2-2").addClass("current");
            sensor_status.bottom_rfid = Math.max(new Date(transformed_data["RFID"+bottom[b]].EVENT_TIME).getTime(), (sensor_status.bottom_rfid || 0));
        }
    }
}



function AnalyseData(data) {
    streaming_data.under_temp = 0;
    streaming_data.over_temp = 0;
    console.log("----------");
    for (var i = 0; i<data.length; i++) {
        if (!transformed_data[data[i].MACHINEID] || new Date(data[i].EVENT_TIME).getTime() > new Date(transformed_data[data[i].MACHINEID].EVENT_TIME).getTime()) {
            transformed_data[data[i].MACHINEID] = data[i];
        }
    }

    CheckSensorStatus(transformed_data);

    var data_names = Object.keys(transformed_data);
    for (var i = 0; i<data_names.length; i++) {
        var item = transformed_data[data_names[i]];
        if (item.EVENT_TYPE === "Prod_Placement"){
            HandleProdPlacement(item);
        }
        else if (item.EVENT_TYPE === "Prod_Detected" && new Date(item.EVENT_TIME).getTime() !== container_reference.last_product_update) {
            //Handle Camera Sensor
            if (item.EVENT_VALUE !== "") {
                container_reference.last_product_update = new Date(item.EVENT_TIME).getTime();
                container_reference.last_product = item.EVENT_VALUE;
                AddAlert({
                    title:"Product Identified as " + item.EVENT_VALUE,
                    id: item.MACHINEID,
                    type: "ready",
                    priority:"Low",
                    sensor:item.MACHINEID,
                    value_threshold:"",
                    detail:""
                });
            }
        }
        else if (item.EVENT_TYPE === "Prod_Temp") {
            //Handle Temp Sensor
            //Adjust can temp if can exists.
            var can_name = item.MACHINEID.slice(0,-1);
            if (can_name === "BLE15") {
                jQuery("#container-temp").text(item.EVENT_VALUE);
                container_reference.temperature = item.EVENT_VALUE;
            }
            else if (can_name === "BLE3") {
                jQuery("#environment-temp").text(item.EVENT_VALUE);
                container_reference.out_temperature = item.EVENT_VALUE;
            } else if (data_cans[can_name]) {
                data_cans[can_name].temperature = parseInt(item.EVENT_VALUE);

                //send notification if necessary
                if (item.EVENT_DESCRIPTION === "Temp High") {
                    streaming_data.over_temp++;
                }
                if (item.EVENT_DESCRIPTION === "Temp Low") {
                    streaming_data.under_temp++;
                }
                if (item.EVENT_DESCRIPTION === "Temp High" && !data_cans[can_name].flags.temp_high) {
                    data_cans[can_name].flags.temp_high = true;//item.EVENT_TIME;
                    AddAlert({
                        title:"High Product Temp",
                        id: item.MACHINEID,
                        type: "temp-high",
                        priority:"Medium",
                        sensor:item.MACHINEID,
                        value_threshold:"72-75 degrees",
                        detail:""
                    });
                }
                if (item.EVENT_DESCRIPTION === "Temp Low" && !data_cans[can_name].flags.temp_low) {
                    data_cans[can_name].flags.temp_low = true;//item.EVENT_TIME;
                    AddAlert({
                        title:"Low Product Temp",
                        id: item.MACHINEID,
                        type: "temp-low",
                        priority:"Medium",
                        sensor:item.MACHINEID,
                        value_threshold:"72-75 degrees",
                        detail:""
                    });
                }
                if (item.EVENT_DESCRIPTION === "Temp OK") {
                    data_cans[can_name].flags.temp_high = false;
                    data_cans[can_name].flags.temp_low = false;
                }
            }
        }
        else if (item.EVENT_TYPE === "Prod_Humidity") {
            var can_name = item.MACHINEID.slice(0,-1);
            
            if (can_name === "BLE15") {
                jQuery("#container-humidity").text(item.EVENT_VALUE);
                container_reference.humidity = item.EVENT_VALUE;
            }
            else if (can_name === "BLE3") {
                jQuery("#environment-humidity").text(item.EVENT_VALUE);
                container_reference.out_humidity = item.EVENT_VALUE;
            }
            else if (data_cans[can_name]) {
                data_cans[can_name].humidity = parseInt(item.EVENT_VALUE);


                //send notification if necessary
                if (item.EVENT_DESCRIPTION === "Humidity High" && !data_cans[can_name].flags.humidity_high) {
                    data_cans[can_name].flags.humidity_high = true;//item.EVENT_TIME;
                    // AddAlert({
                    //     title:"High Product Humidity",
                    //     id: item.MACHINEID,
                    //     type: "humidity-high",
                    //     priority:"Medium",
                    //     sensor:item.MACHINEID,
                    //     value_threshold:"",
                    //     detail:""
                    // });
                }
                if (item.EVENT_DESCRIPTION === "Humidity Low" && !data_cans[can_name].flags.humidity_low) {
                    data_cans[can_name].flags.humidity_low = true;//item.EVENT_TIME;
                    // AddAlert({
                    //     title:"Low Product Humidity",
                    //     id: item.MACHINEID,
                    //     type: "humidity-low",
                    //     priority:"Medium",
                    //     sensor:item.MACHINEID,
                    //     value_threshold:"",
                    //     detail:""
                    // });
                }
                if (item.EVENT_DESCRIPTION === "Humidity OK") {
                    data_cans[can_name].flags.humidity_high = false;
                    data_cans[can_name].flags.humidity_low = false;
                }
            }
        }
        else if (item.EVENT_TYPE === "Prod_Weight") {
            var can_name = location_reference[weight_values[item.MACHINEID]];
            if (data_cans[can_name]) {
                var weight = parseInt(item.EVENT_VALUE);
                data_cans[can_name].weight = CalculateWeight(weight);//Math.max(0,weight < 30000 ? Math.round((weight*-.018 + 9)*10)/10 : 0);
            }
        }
        else if (item.EVENT_TYPE === "Cont_Temp") {
            //Handle Container Temp Sensor

            //send notification if necessary
            // if (item.EVENT_DESCRIPTION = "Temp High" && item.EVENT_TIME != container_reference.last_access) {

            //     AddAlert({
            //         title:"High External Temp",
            //         id: item.MACHINEID,
            //         type: "temp-high",
            //         priority:"Medium",
            //         sensor:"External Fridge Temp",
            //         value_threshold:"72-75 degrees",
            //         detail:""
            //     });
            // }
            jQuery("#container-temp").text(item.EVENT_VALUE);
            container_reference.temperature = item.EVENT_VALUE;

        }
        else if (item.EVENT_TYPE === "Cont_Humidity") {
            //Handle Container Humidity Sensor
            jQuery("#container-humidity").text(item.EVENT_VALUE);
            container_reference.humidity = item.EVENT_VALUE;

        }
        else if (item.EVENT_TYPE === "Cont_Access") {
            //Handle Container Access Scan
            if (item.EVENT_DESCRIPTION === "Container Open") {
                if (!container_reference.opened) {
                    AddAlert({
                        id: item.MACHINEID,
                        title:"Container Opened",
                        type: "opened",
                        priority:"Low",
                        sensor:"Door Sensor",
                        value_threshold:"Opened for less than 59 seconds",
                        detail:""
                    });
                    container_reference.opened = new Date().getTime();

                    if (container_reference.authorized) {
                        HandleOpenDoor();
                    }
                    else {
                        HandleUnauthorizedDoor();
                        AddAlert({
                            title: "Container Unauthorized Access",
                            type: "user",
                            id: item.MACHINEID,
                            priority: "High",
                            sensor: "Door Sensor/External RFID Sensor",
                            value_threshold: "No RFID scan with ID matching authorized user, door opened within 20 seconds",
                            detail: ""
                        });

                    }
                } else if (!container_reference.opened_2 && new Date().getTime() - container_reference.opened > 60000) {
                    AddAlert({
                        id: item.MACHINEID,
                        title:"Container Open 1 Min",
                        type: "opened",
                        priority:"High",
                        sensor:"Door Sensor",
                        value_threshold:"Opened 60 seconds or longer",
                        detail:""
                    });
                    container_reference.opened_2 = true;
                } else if (!container_reference.opened_1 && new Date().getTime() - container_reference.opened > 30000) {
                    AddAlert({
                        id: item.MACHINEID,
                        title:"Container Open 30 Sec",
                        type: "opened",
                        priority:"Medium",
                        sensor:"Door Sensor",
                        value_threshold:"Opened between 30-59 seconds",
                        detail:""
                    });
                    container_reference.opened_1 = true;
                }

            }
            else if (item.EVENT_DESCRIPTION === "Container Closed" && container_reference.opened) {
                AddAlert({
                    id: item.MACHINEID,
                    title:"Container Closed",
                    type: "opened",
                    priority:"Low",
                    sensor:"Door Sensor",
                    value_threshold:"Container closed",
                    detail:""
                });
                HandleCloseDoor();
                container_reference.opened = false;
                container_reference.opened_1 = false;
                container_reference.opened_2 = false;

            }
            else if (item.EVENT_DESCRIPTION === "Access Badge Removed" && item.EVENT_TIME != container_reference.access_removed) {
                container_reference.access_removed = item.EVENT_TIME;
                HandleAuthorizedDoor();
                AddAlert({
                    title:"Access Badge Removed",
                    type: "user",
                    id: item.MACHINEID,
                    priority:"Low",
                    sensor:"Door Sensor/External RFID Sensor",
                    value_threshold:"Access Badge Removed",
                    detail:""
                });
            }
            else if (item.EVENT_DESCRIPTION === "Access Badge Added" && item.EVENT_TIME != container_reference.access_added) {
                container_reference.access_added = item.EVENT_TIME;
                HandleAuthorizedDoor();
                AddAlert({
                    title:"Access Badge Added",
                    type: "user",
                    id: item.MACHINEID,
                    priority:"Low",
                    sensor:"Door Sensor/External RFID Sensor",
                    value_threshold:"Access Badge Added",
                    detail:""
                });
            }
            else if (item.EVENT_DESCRIPTION === "Master Badge Scanned" && item.EVENT_TIME != container_reference.last_access) {
                container_reference.last_access = item.EVENT_TIME;
                container_reference.authorized = true;
                HandleAuthorizedDoor();
                AddAlert({
                    title:"Master Badge Scanned",
                    type: "user",
                    id: item.MACHINEID,
                    priority:"Low",
                    sensor:"Door Sensor/External RFID Sensor",
                    value_threshold:"Master Badge Scanned",
                    detail:""
                });
            }
            else if (item.EVENT_DESCRIPTION === "Authorized Badge Scanned" && item.EVENT_TIME != container_reference.last_access) {
                container_reference.last_access = item.EVENT_TIME;
                container_reference.authorized = true;
                HandleAuthorizedDoor();
                AddAlert({
                    title:"Container Authorized Access",
                    type: "user",
                    id: item.MACHINEID,
                    priority:"Low",
                    sensor:"Door Sensor/External RFID Sensor",
                    value_threshold:"RFID scan with ID matching authorized user, door opened within 5 seconds",
                    detail:""
                });
            }
            else if (item.EVENT_DESCRIPTION === "Unauthorized Badge Scanned" && item.EVENT_TIME != container_reference.last_access) {
                container_reference.last_access = item.EVENT_TIME;
                AddAlert({
                    title:"Container Unauthorized Access",
                    type: "user",
                    id: item.MACHINEID,
                    priority:"High",
                    sensor:"Door Sensor/External RFID Sensor",
                    value_threshold:"No RFID scan with ID matching authorized user, door opened within 5 seconds",
                    detail:""
                });
            }
        }
    }
}


function BuildCanObject (data,s,r,c) {
    var time_to_target = CalculateTimeToTemp(data.temperature);
    var current_date = new Date().getTime();

    // If the can record already exitst, check to see if it's old.
    //      If it's not, just update the data.
    //      If it is, then overwrite it with a new can.
    if (data_cans[data.id] && data_cans[data.id].removed) {
        console.log(current_date - data_cans[data.id].removed < settings.can_removal_threshold);
    }
    if (data_cans[data.id] && Math.abs(data_cans[data.id].temperature - data.temperature) < 10 && (!data_cans[data.id].removed || current_date - data_cans[data.id].removed < settings.can_removal_threshold)) {

        var time_to_expire = CalculateTimeToExpire(data_cans[data.id].product_health, data.temperature, data.target_temperature);

        if (data_cans[data.id].removed) {
            location_reference[data.location_id] = data.id;
            // console.log("Time Diff: " + (current_date - data_cans[data.id].removed));
            AddAlert({
                title:"Item " + data_cans[data.id].display_id + " Added Again",
                id: data.id,
                type: "removed",
                priority:"Low",
                sensor:"Calculated",
                value_threshold:"Item added to inventory again",
                detail:""
            });
        }

        if (data_cans[data.id].status == "no-data") {

        }

        if (data.temperature && data.humidity && data_cans[data.id].product_health === 0) {
            // console.log(data.id+" Expired");
        }

        time_to_target = data_cans[data.id].time_to_target_apl || time_to_target;

        data_cans[data.id].temperature = data.temperature;
        data_cans[data.id].humidity = data.humidity;
        data_cans[data.id].s = s;
        data_cans[data.id].r = r;
        data_cans[data.id].c = c;
        data_cans[data.id].removed = undefined;
        data_cans[data.id].time_to_target_number = time_to_target;
        data_cans[data.id].time_to_target = data_cans[data.id].product_health === 0 || !data.temperature ? "N/A" : time_to_target > 0 ? time_to_target + " minutes" : "Ready";
        data_cans[data.id].status = data.temperature && data.humidity ? data_cans[data.id].product_health === 0 ? "expired" : time_to_target > 0 ? "ready-soon" : "ready" : "no-data";
        data_cans[data.id].time_to_expire = time_to_expire;
        data_cans[data.id].expiration = data_cans[data.id].product_health === 0 ? "Expired" : Math.floor(time_to_expire / 60) + " hours " + Math.ceil(time_to_expire % 60) + " minutes";


        if (data_cans[data.id].product === "Unidentified") {
            data_cans[data.id].product = container_reference.last_product_update && Math.abs(container_reference.last_product_update - new Date(data.EVENT_TIME).getTime()) < settings.camera_product_id_threshold ? container_reference.last_product : "Unidentified";
        }

    } else {
        data.display_id = total_products++;

        var time_to_expire = CalculateTimeToExpire(data.product_health, data.temperature, data.target_temperature);
        // Generate a new can record.
        if (data_cans[data.id]) {
            console.log("NEW!!");
        }

        AddAlert({
            title:"Item " + data.display_id + " Added",
            id: data.id,
            type: "added",
            priority:"Medium",
            sensor:"Calculated",
            value_threshold:"Item added to inventory",
            detail:""
        });


        location_reference[data.location_id] = data.id;
        data_cans[data.id] = {
            s:s,
            r:r,
            c:c,
            display_id: data.display_id,
            rfid: data.rfid,
            product: container_reference.last_product_update && new Date(data.EVENT_TIME).getTime() - container_reference.last_product_update < settings.camera_product_id_threshold ? container_reference.last_product : "Unidentified",
            status: data.temperature && data.humidity ? data.product_health === 0 ? "expired" : data.temperature <= data.target_temperature ? "ready" : "ready-soon" : "no-data",
            id: data.id,
            time_in_container: new Date(),
            target_temperature: data.target_temperature,
            humidity: data.humidity,
            product_health: data.product_health,
            temperature: data.temperature,
            time_to_target_number: time_to_target,
            time_to_target: data.product_health === 0 ? "N/A" : time_to_target > 1 ? time_to_target + " minutes" : "Ready",
            time_to_expire: time_to_expire,
            expiration: data.product_health === 0 ? "Expired" : Math.floor(time_to_expire / 60) + " hours " + Math.ceil(time_to_expire % 60) + " minutes",
            // dispensed_from: RandomNumber(1,100),
            flags: {}
        };
    }
}

function Handle_hightemp (results) {

    console.log("hightemp");
    console.log(results);
}
function Handle_lowtemp (results) {

    console.log("lowtemp");
    console.log(results);
}
function Handle_allalerts (results) {

    console.log("allalerts");
    console.log(results);

    var alerts = [];
    for (var i=0; i<results.entries.length; i++) {
        data = results.entries[i];
        var new_alert = {
            title: data.title,
            type: data.type,
            priority: data.priority,
            sensor: data.sensor,
            itemId: data.id,
            value_threshold: data.value_threshold,
            detail: data.detail || ""
        };

        var date_string = new Date().toString();
        new_alert.date_time = date_string.substring(0, date_string.indexOf("GMT"));
    }

    ClearAlertsLog();
    CreateAlertsLog();
}
function Handle_alerts (results) {

    console.log("alerts");
    console.log(results);
}
function Handle_tempexternal (entries) {
    console.log("tempexternal");
    console.log(entries);
    if (entries.results && entries.results[0]) {
        item = entries.results[entries.results.length-1];

        jQuery("#environment-temp").text(item.TEMP_VALUE);
        container_reference.out_temperature = item.TEMP_VALUE;

        jQuery("#environment-humidity").text(item.HUMD_VALUE);
        container_reference.out_humidity = item.HUMD_VALUE;
    }
}
function Handle_tempinternal (entries) {
    console.log("tempinternal");
    console.log(entries);
    if (entries.results && entries.results[0]) {
        item = entries.results[entries.results.length-1];

        jQuery("#container-temp").text(item.TEMP_VALUE);
        container_reference.temperature = item.TEMP_VALUE;

        jQuery("#container-humidity").text(item.HUMD_VALUE);
        container_reference.humidity = item.HUMD_VALUE;
    }

}
function Handle_dooralerts (results) {

    console.log("dooralerts");
    console.log(results);
}
var transformed_carriers = {};
function Handle_carrierdata (entries) {
    var results = entries.results;
    console.log("carrierdata");
    console.log(results);

    for (var i = 0; i<results.length; i++) {
        if (!transformed_carriers[results[i].CARRIER_ID] || new Date(results[i].TIME_STAMP).getTime() > new Date(transformed_carriers[results[i].CARRIER_ID].TIME_STAMP).getTime()) {
            transformed_carriers[results[i].CARRIER_ID] = results[i];
        }
    }
    var data_names = Object.keys(transformed_carriers);
    for (var i = 0; i<data_names.length; i++) {
        //Handle Temp Sensor
        //Adjust can temp if can exists.
        var item = transformed_carriers[data_names[i]];
        var can_name = item.CARRIER_ID.slice(0,-1);
        if (data_cans[can_name]) {
            data_cans[can_name].temperature = parseInt(item.TEMP_VALUE);
        }
        else if (can_name === "BLE15") {
            jQuery("#container-temp").text(item.TEMP_VALUE);
            container_reference.temperature = item.TEMP_VALUE;
        }
        else if (can_name === "BLE3") {
            jQuery("#environment-temp").text(item.TEMP_VALUE);
            container_reference.out_temperature = item.TEMP_VALUE;
        }

        ///HANDLE HUMIDITY
        if (data_cans[can_name]) {
            data_cans[can_name].humidity = parseInt(item.HUMD_VALUE);
        }
        else if (can_name === "BLE15") {
            jQuery("#container-humidity").text(item.HUMD_VALUE);
            container_reference.humidity = item.HUMD_VALUE;
        }
        else if (can_name === "BLE3") {
            jQuery("#environment-humidity").text(item.HUMD_VALUE);
            container_reference.out_humidity = item.HUMD_VALUE;
        }
    }
}


function Handle_placedcarriers (entries) {
    var results = entries.results;
    console.log("placedcarriers");
    console.log(entries);
    // data_cans = {};



    for (var i = 0; i<results.length; i++) {
        var item = results[i];
        var rfid = sensor_rfid_values[item.CARRIER_ID];
        var s = rfid_sensor_positions[item.POSITION_ID].s;
        var r = rfid_sensor_positions[item.POSITION_ID].r;
        var c = rfid_sensor_positions[item.POSITION_ID].c;
        var data = {
            id: item.CARRIER_ID,//,
            ble_id: sensor_rfid_values[item.CARRIER_ID],
            display_id: item.PRODUCT,
            location_id: item.POSITION_ID,
            EVENT_TIME: item.TIME_STAMP,
            target_temperature: settings.target_temperature,
            temperature: item.TEMP_VALUE,
            humidity: item.HUMD_VALUE,
            weight: item.WEIGHT_VALUE,
            rfid: item.CARRIER_ID,
            time_in_container: new Date().getTime() - new Date(item.TIME_STAMP).getTime()
        };


        // console.log(item.CARRIER_ID+": "+item.POSITION_ID);


        var time_to_target = item.APL_TIME;//CalculateTimeToTemp(data.temperature);
        var current_date = new Date().getTime();



        location_reference[data.location_id] = data.id;
        var new_can = false;

        if (!data_cans[data.location_id]) {
            new_can = true;
        }

        data.product_health = data_cans[data.location_id] ? data_cans[data.location_id].product_health : 100;



        var time_to_expire = CalculateTimeToExpire(data.product_health, data.temperature, data.target_temperature);
        var time_to_target = CalculateTimeToTemp(data.temperature);

        data_cans[data.location_id] = {
            s:s,
            r:r,
            c:c,
            display_id: data.display_id,
            rfid: data.rfid,
            product: container_reference.last_product_update && new Date(data.EVENT_TIME).getTime() - container_reference.last_product_update < settings.camera_product_id_threshold ? container_reference.last_product : "Unidentified",
            status: data.temperature && data.humidity ? data.product_health === 0 ? "expired" : data.temperature <= data.target_temperature ? "ready" : "ready-soon" : "no-data",
            id: data.id,
            target_temperature: data.target_temperature,
            weight: data.weight,
            humidity: data.humidity,
            product_health: data.product_health,
            time_in_container_number: data.time_in_container,
            time_in_container: Math.round(data.time_in_container / 1000 / 60) + " minutes",
            temperature: data.temperature,
            time_to_target_number: time_to_target,
            time_to_target: data.product_health === 0 ? "N/A" : time_to_target > 1 ? time_to_target + " minutes" : "Ready",
            time_to_expire: time_to_expire,
            expiration: data.product_health === 0 ? "Expired" : Math.floor(time_to_expire / 60) + " hours " + Math.ceil(time_to_expire % 60) + " minutes",
            // dispensed_from: RandomNumber(1,100),
            flags: {}
        };

        if (new_can) {
            for (var j=0; j<Math.round(data.time_in_container / 1000 / 60); j++) {
                if (data_cans[data.location_id].product_health>0) {
                    UpdateCanHealth(data_cans[data.location_id]);
                }
            }
        }
    }
}


function UpdateAPL (results) {
    /*
    [
        {
        "POS_ID":"POS_11",
        "EVENT_TIME":"2018-09-13T09:01:51",
        "APL_TIME":"30"
        },
        {
        "POS_ID":"POS_12",
        "EVENT_TIME":"2018-09-13T09:02:00",
        "APL_TIME":"19"
        },
        {
        "POS_ID":"POS_13",
        "EVENT_TIME":"2018-09-13T09:02:06",
        "APL_TIME":"11"
        },
        {
        "POS_ID":"POS_14",
        "EVENT_TIME":"2018-09-13T09:02:15",
        "APL_TIME":"14"
        }
    ]
    */

    var number = 0;
    var time_to_target = 0;
    for (var i=0;i<results.length;i++) {
        number = results[i]["POS_ID"].slice(4,6);
        time_to_target = parseInt(results[i]["APL_TIME"],10);

        if (location_reference["RFID"+number] && data_cans[location_reference["RFID"+number]]) {
            data_cans[location_reference["RFID"+number]].time_to_target_apl = time_to_target;
            data_cans[location_reference["RFID"+number]].time_to_target_number = time_to_target;
            data_cans[location_reference["RFID"+number]].time_to_target = data_cans[location_reference["RFID"+number]].product_health === 0 ? "N/A" : time_to_target > 1 ? time_to_target + " minutes" : "Ready";

            if (!data_cans[location_reference["RFID"+number]].time_to_target_chart) {
                data_cans[location_reference["RFID"+number]].time_to_target_chart = [];
            }

            if (results[i]["EVENT_TIME"] !== data_cans[location_reference["RFID"+number]].time_to_target_chart_last_date) {
                data_cans[location_reference["RFID"+number]].time_to_target_chart;
                data_cans[location_reference["RFID"+number]].time_to_target_chart_last_date = results[i]["EVENT_TIME"];
            }
        }
    }

    UpdateCans(true);

}

function GetAPL () {

    jQuery.ajax({
        method: "GET",
        url: "http://" + window.location.hostname + ":3000/apl",
        headers: {
            // Authorization: "Bearer " + user.token
        }
    }).done(function (entries) {
        if (entries.results) {
            UpdateAPL(entries.results);
            console.log("APL Success");
        } else {

            // jQuery("#error-modal").addClass("show");
            // jQuery("#loading-modal").removeClass("show");
        }

    }).fail(function (error) {
        console.error(error);
        // jQuery("#error-modal").addClass("show");
        // jQuery("#loading-modal").removeClass("show");
    });
}

function GetData (callback) {
//     http:// 10.5.1.2:3000/hightemp          --- High Temp Alarm
//     http:// 10.5.1.2:3000/lowtemp           --- Low Temp Alarm
//     http:// 10.5.1.2:3000/carrierdata       --- View of last X seconds of carrier temp/humd data
//     http:// 10.5.1.2:3000/placedcarriers    --- View of placed carriers
//     http:// 10.5.1.2:3000/dooralerts        --- View of door/rfid badge access alerts
    // var finished = 0;
    // function return_function () {
    //     if (finished >= 4) {
    //         callback();
    //     }
    // }


    // jQuery.ajax({
    //     method: "GET",
    //     url: "http://" + window.location.hostname + ":3000/alerts",
    //     headers: {
    //         // Authorization: "Bearer " + user.token
    //     }
    // }).done(function (entries) {
    //     finished++;
    //     if (entries) {
    //         Handle_alerts(entries);
    //     }
    //     else {
    //         console.log("error loading hightemp");
    //     }
    //     return_function();
    // }).fail(function (error) {
    //     finished++;
    //     console.error(error);
    //     return_function();
    // });

    // // jQuery.ajax({
    // //     method: "GET",
    // //     url: "http://" + window.location.hostname + ":3000/carrierdata",
    // //     headers: {
    // //         // Authorization: "Bearer " + user.token
    // //     }
    // // }).done(function (entries) {
    // //     finished++;
    // //     if (entries) {
    // //         Handle_carrierdata(entries);
    // //     }
    // //     else {
    // //         console.log("error loading carrierdata");
    // //     }
    // //     return_function();
    // // }).fail(function (error) {
    // //     finished++;
    // //     console.error(error);
    // //     return_function();
    // // });

    // jQuery.ajax({
    //     method: "GET",
    //     url: "http://" + window.location.hostname + ":3000/placedcarriers",
    //     headers: {
    //         // Authorization: "Bearer " + user.token
    //     }
    // }).done(function (entries) {
    //     finished++;
    //     if (entries) {
    //         Handle_placedcarriers(entries);
    //     }
    //     else {
    //         console.log("error loading placedcarriers");
    //     }
    //     return_function();
    // }).fail(function (error) {
    //     finished++;
    //     console.error(error);
    //     return_function();
    // });

    // // jQuery.ajax({
    // //     method: "GET",
    // //     url: "http://" + window.location.hostname + ":3000/dooralerts",
    // //     headers: {
    // //         // Authorization: "Bearer " + user.token
    // //     }
    // // }).done(function (entries) {
    // //     finished++;
    // //     if (entries) {
    // //         Handle_dooralerts(entries);
    // //     }
    // //     else {
    // //         console.log("error loading dooralerts");
    // //     }
    // //     return_function();
    // // }).fail(function (error) {
    // //     finished++;
    // //     console.error(error);
    // //     return_function();
    // // });

    // jQuery.ajax({
    //     method: "GET",
    //     url: "http://" + window.location.hostname + ":3000/tempinternal",
    //     headers: {
    //         // Authorization: "Bearer " + user.token
    //     }
    // }).done(function (entries) {
    //     finished++;
    //     if (entries) {
    //         Handle_tempinternal(entries);
    //     }
    //     else {
    //         console.log("error loading tempinternal");
    //     }
    //     return_function();
    // }).fail(function (error) {
    //     finished++;
    //     console.error(error);
    //     return_function();
    // });

    // jQuery.ajax({
    //     method: "GET",
    //     url: "http://" + window.location.hostname + ":3000/tempexternal",
    //     headers: {
    //         // Authorization: "Bearer " + user.token
    //     }
    // }).done(function (entries) {
    //     finished++;
    //     if (entries) {
    //         Handle_tempexternal(entries);
    //     }
    //     else {
    //         console.log("error loading tempexternal");
    //     }
    //     return_function();
    // }).fail(function (error) {
    //     finished++;
    //     console.error(error);
    //     return_function();
    // });
    jQuery.ajax({
        method: "GET",
        url: "http://" + window.location.hostname + ":3000/",
        headers: {
            // Authorization: "Bearer " + user.token
        }
    }).done(function (entries) {
        if (entries.results) {
            callback(entries.results);

            GetAPL();

        } else {//if (!data_cans) {
            jQuery("#error-modal").addClass("show");
            jQuery("#loading-modal").removeClass("show");
        }

    }).fail(function (error) {
        console.error(error);
        // if (!data_cans) {
            jQuery("#error-modal").addClass("show");
            jQuery("#loading-modal").removeClass("show");
        // }
    });


    //callback(activity_history);
}

function GetGlobalData () {

    jQuery.ajax({
        method: "GET",
        url: "http://" + window.location.hostname + ":3000/get",
        headers: {
            // Authorization: "Bearer " + user.token
        }
    }).done(function (raw_data) {
        var entries = JSON.parse(raw_data);
        console.log(entries);

        if (entries.data_cans) {
            data_cans = entries.data_cans;
            location_reference = entries.location_reference;
            active_alerts = entries.active_alerts;

            transformed_data = entries.transformed_data;
            container_reference = entries.container_reference;
            streaming_data = entries.streaming_data;
            dispensed_today = entries.dispensed_today;
            dispensed_record = entries.dispensed_record;
            total_products = entries.total_products;

            UpdateAlerts();
            UpdateCans();
        } else {
            console.log("no Data Retrieved");
        }



    }).fail(function (error) {
        console.error(error);
    });
}

function UpdateData () {
    var body_object = {
        data_cans: data_cans,
        location_reference: location_reference,
        active_alerts: active_alerts,
        transformed_data: transformed_data,
        container_reference: container_reference,
        streaming_data: streaming_data,
        dispensed_today: dispensed_today,
        dispensed_record: dispensed_record,
        total_products: total_products
    };
    var body_string = JSON.stringify(body_object) + '';
    jQuery.ajax({
        method: "POST",
        url: "http://" + window.location.hostname + ":3000/post",
        // contentType: 'application/json',
        dataType: 'json',
        data: body_string,
        headers: {
            // Authorization: "Bearer " + user.token
            "Content-Type": 'application/json'
        }
    }).done(function (entries) {
        console.log("posted");
    }).fail(function (error) {
        if (error.status === 200) {
            console.log("post success");
        } else {
            console.log("post failed");
        }
    });
}

function OnLoad (data) {
    jQuery("#loading-modal").removeClass("show");
    UpdateInventory();
    UpdateGeospatial(settings.geospatial_route);
    // UpdateStreaming();
    UpdateContainer();
    UpdatePredictive();

    StartSliders();
    StartAlerts();
    StartLegal();

    StartWebApp(data_cans);

    StartTimeAtLocation(settings.geospatial_route);

    ClearCans();
    CreateCans();

    setInterval(function () {
        GetData(function(activity) {
            AnalyseData(activity);
            // console.log("Update");

            UpdateCans(true);
            UpdateStreaming(activity);
        });
    }, settings.get_data_rate);

    // setInterval(function () {
    //     UpdateData();
    // }, settings.post_data_rate);

    setInterval(function () {
        UpdateCans();
    }, settings.can_update_rate);

    if (settings.use_backup_videos) {
        StartVideoControls();
    }


    GetGlobalData();
}

function StartApp () {
    if (settings.use_backup_videos) {
        StartVideo();
    }
    // GetMap();

    // setTimeout(function () {
    GetData(function(activity) {
        AnalyseData(activity);
        OnLoad();
    });
    // }, 2000);


    jQuery("#demo-remove").click(function() {
        activity_history.push({
            "MACHINEID": "RFID11",
            "EVENT_TIME": "Apr 22, 2018 12:03:35.867 PM",
            "EVENT_TYPE": "Prod_Placement",
            "EVENT_DESCRIPTION": "RFID Not Found",
            "EVENT_VALUE": ""
        });

        demo_removed_id = data_cans[location_reference["RFID11"]].rfid;

    });
}


var demo_removed_id;
jQuery(document).ready(function () {



    // jQuery("#loading-modal").removeClass("show");

    StartApp();


});


//////////MAP!!!!


var map;
var pin;
var start_pin;
var directionsManager;
var directionWaypointLayer;
var pin_animation_data;
var pin_interval;


function StartVideo () {
  LoadVideo(settings.geospatial_videos[settings.geospatial_route]);
  StartVideoControls();
  jQuery("#video").removeClass("hide");
  // jQuery("#video-controls").removeClass("hide");
  jQuery("#myMap").addClass("hide");
  jQuery("#route-title").addClass("hide");
}

function GetPinAnimationData() {
    jQuery.ajax({
        method: "GET",
        url: "http://" + window.location.hostname + ":3000/" + settings.geo_animation_service[settings.geospatial_route],
        headers: {
            // Authorization: "Bearer " + user.token
        }
    }).done(function (raw_data) {
        pin_animation_data = raw_data;

        if (start_pin) {
            StartPin(raw_data);
        }

    }).fail(function (error) {
        console.error(error);
        StartVideo();
    });
}

function StartPin (lat_lon_data) {
    var location = {};
    var frame = 0;
    var color;
    if (pin_interval) {
        clearInterval(pin_interval);
    }
    var text_box = jQuery("#route-title");
    var enroute = "";
    pin_interval = setInterval(function () {
        if (frame < lat_lon_data.results.length) {
            location = lat_lon_data.results[frame];
            if (location.OnRoute === "Yes" && !enroute) {
                text_box.text("en route");
                text_box.removeClass("off-route");
                enroute = true;
            } else if (location.OnRoute !== "Yes" && enroute) {
                text_box.text("off route");
                text_box.addClass("off-route");
                enroute =false;
            }
            CreatePin(location.ActualLAT,location.ActualLON,location.OnRoute === "Yes" ? "Black" : "Red");
            frame++;
        }
        else {
            clearInterval(pin_interval);
            text_box.text("arrived");
            text_box.removeClass("off-route");
            text_box.addClass("arrived");
        }
    }, 10);
}


function CreatePin (lat,lon,color) {
    // geo_orlando

    map.entities.clear();
    pin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(lat, lon), {
        icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="18" xml:space="preserve"><circle cx="9" cy="9" r="7" style="stroke-width:2;stroke:#ffffff;fill:{color};"/></svg>',
        color: color,
        text: "",
        anchor: new Microsoft.Maps.Point(9, 9)
    });

    map.entities.push(pin);
}



function GetMap()
{

    jQuery("#route-title").text("rendering route");

    var route = data_geospatial[settings.geospatial_route];

    map = new Microsoft.Maps.Map('#myMap', {
        showLocateMeButton: false,
        disableScrollWheelZoom: true,
        disablePanning: true,
        disableMapTypeSelectorMouseOver: true,
        disableKeyboardInput: true,
        disableStreetside: true,
        // disableZooming: true,
        // showDashboard: false,
        showMapTypeSelector: false,
        showZoomButtons: true,
        credentials: 'Ah_C8OJJu8wnNX50rGHf8_OYKonuhZ-CfLQ-kXS-4tI-QsTN9pkLPPfgZgKigwa8'
    });
    directionWaypointLayer = new Microsoft.Maps.Layer({

        zIndex: 0,
    });
    map.layers.insert(directionWaypointLayer);

    //Load the directions module.
    Microsoft.Maps.loadModule('Microsoft.Maps.Directions', function () {
        //Create an instance of the directions manager.
        directionsManager = new Microsoft.Maps.Directions.DirectionsManager(map);

        //Create waypoints to route between.
        var seattleWaypoint = new Microsoft.Maps.Directions.Waypoint({ address: route.from.address });
        directionsManager.addWaypoint(seattleWaypoint);

        var workWaypoint = new Microsoft.Maps.Directions.Waypoint({ address: route.to.address });//, location: new Microsoft.Maps.Location(47.64, -122.1297) });
        directionsManager.addWaypoint(workWaypoint);

        directionsManager.setRequestOptions({
            routeMode: Microsoft.Maps.Directions.RouteMode.driving,
            routeOptimization: Microsoft.Maps.Directions.RouteOptimization.shortestDistance,
            routeDraggable: false,
            waypointPushpinOptions:{visible:false},
            viapointPushpinOptions:{visible:false}
        });

        //Specify the element in which the itinerary will be rendered.
        // directionsManager.setRenderOptions({ itineraryContainer: '#directionsItinerary' });


        directionsManager.setRenderOptions({
            firstWaypointPushpinOptions: { visible: false },
            lastWaypointPushpinOptions: { visible: false },
            waypointPushpinOptions: { visible: false }
        });
        Microsoft.Maps.Events.addHandler(directionsManager, 'directionsUpdated', directionsUpdated);

        //Specify the element in which the itinerary will be rendered.
        // directionsManager.setRenderOptions({ itineraryContainer: '#directionsItinerary' });

        //Calculate directions.
        directionsManager.calculateDirections();

        GetPinAnimationData();
    });
}

function directionsUpdated(e) {
    directionWaypointLayer.clear();
    if (e.route && e.route.length > 0) {
        var route = e.route[0];
        var waypointCnt = 0;
        var stepCount = 0;
        var waypointLabel = "ABCDEFGHIJKLMNOPQRSTYVWXYZ";
        var wp = [];
        var step;
        var isWaypoint;
        var waypointColor;

        for (var i = 0; i < route.routeLegs.length; i++) {
            for (var j = 0; j < route.routeLegs[i].itineraryItems.length; j++) {
                stepCount++;
                isWaypoint = true;
                step = route.routeLegs[i].itineraryItems[j];
                if (j == 0) {
                    if (i == 0) {
                        //Start Endpoint, make it green.
                        waypointColor = '#008f09';
                    } else {
                        //Midpoint Waypoint, make it gray,
                        waypointColor = '#737373';
                    }
                } else if (i == route.routeLegs.length - 1 && j == route.routeLegs[i].itineraryItems.length - 1) {
                    //End waypoint, make it red.
                    waypointColor = '#d60000';

                } else {
                    //Instruction step
                    isWaypoint = false;
                }
                if (isWaypoint) {
                    pin = new Microsoft.Maps.Pushpin(step.coordinate, {
                        icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="52" height="49.4" viewBox="0 0 37 35" xml:space="preserve"><circle cx="12" cy="12" r="7" style="stroke-width:0;fill:#000000;"/><text x="12" y="16" style="font-size:11px;font-family:arial;fill:#ffffff;" text-anchor="middle">{text}</text></svg>',
                        anchor: new Microsoft.Maps.Point(12, 12),
                        color: waypointColor,
                        text: waypointLabel[waypointCnt]    //Give waypoints a letter as a label.
                    });
                    //Store the instruction information in the metadata.
                    pin.metadata = {
                        description: step.formattedText,
                        infoboxOffset: new Microsoft.Maps.Point(-30, 25)
                    };
                    waypointCnt++;
                    wp.push(pin);
                } else {
                    // //Instruction step, make it a red circle with its instruction index.
                    // pin = new Microsoft.Maps.Pushpin(step.coordinate, {
                    //     icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="17" viewBox="0 0 36 34" xml:space="preserve"><circle cx="16" cy="16" r="14" style="fill:{color}" /><text x="16" y="21" style="font-size:16px;font-family:arial;fill:#ffffff;" text-anchor="middle">{text}</text></svg>',
                    //     anchor: new Microsoft.Maps.Point(9, 9),
                    //     color: '#d60000',
                    //     text: stepCount + ''
                    // });
                    // //Store the instruction information in the metadata.
                    // pin.metadata = {
                    //     description: step.formattedText,
                    //     infoboxOffset: new Microsoft.Maps.Point(0, 0)
                    // };
                }

                // wp.push(pin);
            }
        }
        //Reverse the order of the pins so that when rendered the last waypoints in the route are on top.
        wp.reverse();
        //Add the pins to the map.
        directionWaypointLayer.add(wp);
    }
}
