var settings = {

    /*

    0 - Route 1:
    Orlando
    Orlando International Airport (One Jeff Fuqua Blvd, Orlando, FL 32827)
    Orlando County Convention Center (9800 International Drive, Orlando, FL 32819-8111)

    1 - Route 2:
    San Francisco
    San Francisco International Airport (780 S Airport Blvd, San Francisco, CA 94128)
    SAP Palo Alto office (3410 Hillview Avenue, Palo Alto CA 94304)

    2 - Route 3:
    Las Vagas

    */

// GEOSPATIAL SETTINGS

    // Determines which route data to use (see above) the value is 0-2
    geospatial_route: 0,
    geospatial_videos: [
        "./SAP_Map_Orlando.mp4",
        "./SAP_Map_SanFran.mp4",
        "./SAP_Map_Vegas.mp4"
    ],
    use_backup_videos: true,
    geo_animation_service: [
        "geo_orlando",
        "geo_san",
        "geo_lasvegas"
    ],

    //The arrival date
    route_arrival_date: new Date("09-18-2018 6:00:00"),





// SLIDER SETTINGS

    // How fast the container switches slides (milliseconds)
    "display-container_delay": 18000,
    "pie-slider_delay": 10000,
    // How fast the center panel switches slides (milliseconds)
    // "slider-info_delay": 31000,





// TIMING SETTINGS

    // How often to call the server (milliseconds)
    get_data_rate: 3000,
    post_data_rate: 5000,
    // How often to apply can health decay (milliseconds)
    can_update_rate: 60000,
    // How often the alert chart updates (minutes)
    alert_history_interval: 5,
    // How long until an authorized access expires
    door_time: 20000,
    door_image_refresh_rate: 500,
    // How long to apply an identified product to a new can. (milliseconds)
    camera_product_id_threshold: 15000,

    last_sensor_threshold: 120000,
    // How long after a product is removed to consider it a new product (milliseconds)
    can_removal_threshold: 30000,
    //Heat map updates every number of milliseconds
    heat_map_update: 6000,

// CONTAINER SETTINGS
    // Default temperature
    default_fridge_temp: 8.75,
    // Target temperature
    target_temperature: 13,
    // k used for decay formula
    temp_k: -0.0081,
    // Container position info
    container_stats: {
        slides: 3,
        rows: 2,
        columns: 4
    }
};



var data_geospatial = [
	{
		from: {
			address: "Orlando International Airport (One Jeff Fuqua Blvd, Orlando, FL 32827)",
			temp: "71",
			latitude: "28.424827",
			longitude: "-81.469436"
		},
		to: {
			address: "Orlando County Convention Center (9800 International Drive, Orlando, FL 32819-8111)",
			temp: "65",
			latitude: "28.424827",
			longitude: "-81.469436",
			zip_code: "32827",
			city: "Orlando, FL",
			county: "Orange County, FL",
			population_density: "389 people per sq mi",
			cost_of_living_index: "93.5",
			median_household_size: "2.8 people",
			median_household_income: "$38,298"
		},
		// time_at_location: "27 hours, 13 minutes",
		travel_time_to_location: "23 minutes",
		travel_distance_to_location: "12.4 miles",
		travel_distance_to_location_km: "(21.4 kilometers)"
	},

	{
		from: {
			address: "San Francisco International Airport (780 S Airport Blvd, San Francisco, CA 94128)",
			temp: "71",
			latitude: "37.615223",
			longitude: "-122.389977"
		},
		to: {
			address: "SAP Palo Alto office (3410 Hillview Avenue, Palo Alto CA 94304)",
			temp: "65",
			latitude: "37.3988271",
			longitude: "-122.1465488",
			zip_code: "94304",
			city: "Palo Alto, CA",
			county: "Santa Clara County, CA",
			population_density: "2,831 people per sq mi",
			cost_of_living_index: "175.0",
			median_household_size: "2.4 people",
			median_household_income: "$132,673"
		},
		// time_at_location: "27 hours, 13 minutes",
		travel_time_to_location: "27 minutes",
		travel_distance_to_location: "27.4 miles",
		travel_distance_to_location_km: "(44.9 kilometers)"
	},

	{
		from: {
			address: "5757 Wayne Newton Blvd, Las Vegas, NV 89119",
			temp: "96",
			latitude: "36.0838937",
			longitude: "-115.1559106"
		},
		to: {
			address: "3355 Las Vegas Boulevard South, Las Vegas, NV 89109",
			temp: "96",
			latitude: "36.1229546",
			longitude: "-115.1748677",
			zip_code: "89109",
			city: "Las Vegas, NV",
			county: "Clark County, NV",
			population_density: "4,366 people per sq mi",
			cost_of_living_index: "113",
			median_household_size: "2.7 people",
			median_household_income: "$58,432"
		},
		// time_at_location: "27 hours, 13 minutes",
		travel_time_to_location: "14 minutes",
		travel_distance_to_location: "4.3 miles",
		travel_distance_to_location_km: "(6.9 kilometers)"
	}



];


// This table corrilates RFID tag values to their corrisponding BLE sensor.
// var sensor_rfid_values = {
//     "c0c0d9d9": "BLE8",
//     "71d31fa3": "BLE13",
//     "e1a16dd9": "BLE24",
//     "fd2fdbd9": "BLE6",
//     "726e4729": "BLE4",
//     "ea0dd9ab": "BLE11",
//     "d393a4b9": "BLE23",
//     "f3a94729": "BLE14",
//     "b092a4b9": "BLE9",
//     "563fdbd9": "BLE18",
//     "0d74dbd9": "BLE21",
//     "bae37d63": "BLE2",
//     "c60b4d79": "BLE10",
//     "c27dd9d9": "BLE20",
//     "8673718b": "BLE17",
//     "83d9a0b9": "BLE22",
//     "99c72d02": "BLE16",
//     "b5722e02": "BLE7",
//     "3b612b02": "BLE19"
// };


var weight_values = {
    "FSR11": "RFID11",
    "FSR12": "RFID12",
    "FSR13": "RFID13",
    "FSR14": "RFID14",
    "FSR15": "RFID15",
    "FSR16": "RFID16",
    "FSR17": "RFID17",
    "FSR18": "RFID18",
    "FSR19": "RFID19",
    "FSR20": "RFID20",
    "FSR21": "RFID21",
    "FSR22": "RFID22",
    "FSR23": "RFID23",
    "FSR24": "RFID24",
    "FSR25": "RFID25",
    "FSR26": "RFID26",
    "FSR27": "RFID27",
    "FSR28": "RFID28",
    "FSR29": "RFID29",
    "FSR30": "RFID30"
};

var sensor_rfid_values = {
    "fb9b50d3": "BLE1",
    "fad85ad3": "BLE2",
    "49f0b3c3": "BLE3",
    "33d74cd3": "BLE4",
    "7d426183": "BLE5",
    "06b34bd3": "BLE6",
    "9e3c5bd3": "BLE7",
    "bc67a1d3": "BLE8",
    "50094f83": "BLE9",
    "1e745ad3": "BLE10",
    "1a0f50d3": "BLE11",
    "391d4cd3": "BLE12",
    "91fdb4c3": "BLE13",
    "62165c83": "BLE14",
    "107a4bd3": "BLE15",
    "ebd654d3": "BLE16",
    "8325a2d3": "BLE17",
    "06794bd3": "BLE18",
    "d4005d83": "BLE19",
    "9cce5083": "BLE20"
};

var container_id_values = {
    "fb9b50d3": "CID_01",
    "fad85ad3": "CID_02",
    "49f0b3c3": "CID_03",
    "33d74cd3": "CID_04",
    "7d426183": "CID_05",
    "06b34bd3": "CID_06",
    "9e3c5bd3": "CID_07",
    "bc67a1d3": "CID_08",
    "50094f83": "CID_09",
    "1e745ad3": "CID_10",
    "1a0f50d3": "CID_11",
    "391d4cd3": "CID_12",
    "91fdb4c3": "CID_13",
    "62165c83": "CID_14",
    "107a4bd3": "CID_15",
    "ebd654d3": "CID_16",
    "8325a2d3": "CID_17",
    "06794bd3": "CID_18",
    "d4005d83": "CID_19",
    "9cce5083": "CID_20"
};





// This object declares the locations of each of the RFID sensors
// s - slide
// r - row
// c - column
// var rfid_sensor_positions = {
//     "POS_10": {
//         s: 0,
//         r: 0,
//         c: 0
//     },
//     "POS_11": {
//         s: 0,
//         r: 0,
//         c: 1
//     },
//     "POS_12": {
//         s: 0,
//         r: 0,
//         c: 2
//     },
//     "POS_13": {
//         s: 0,
//         r: 0,
//         c: 3
//     },
//     "POS_14": {
//         s: 0,
//         r: 1,
//         c: 0
//     },
//     "POS_15": {
//         s: 0,
//         r: 1,
//         c: 1
//     },
//     "POS_16": {
//         s: 0,
//         r: 1,
//         c: 2
//     },
//     "POS_17": {
//         s: 0,
//         r: 1,
//         c: 3
//     },

//     "POS_18": {
//         s: 1,
//         r: 0,
//         c: 0
//     },
//     "POS_19": {
//         s: 1,
//         r: 0,
//         c: 1
//     },
//     "POS_20": {
//         s: 1,
//         r: 0,
//         c: 2
//     },
//     "POS_21": {
//         s: 1,
//         r: 0,
//         c: 3
//     },
//     "POS_22": {
//         s: 1,
//         r: 1,
//         c: 0
//     },
//     "POS_23": {
//         s: 1,
//         r: 1,
//         c: 1
//     },
//     "POS_24": {
//         s: 1,
//         r: 1,
//         c: 2
//     },
//     "POS_25": {
//         s: 1,
//         r: 1,
//         c: 3
//     },

//     "POS_26": {
//         s: 2,
//         r: 0,
//         c: 0
//     },
//     "POS_27": {
//         s: 2,
//         r: 0,
//         c: 1
//     },
//     "POS_28": {
//         s: 2,
//         r: 0,
//         c: 2
//     },
//     "POS_29": {
//         s: 2,
//         r: 0,
//         c: 3
//     },
//     "POS_30": {
//         s: 2,
//         r: 1,
//         c: 0
//     },
//     "POS_31": {
//         s: 2,
//         r: 1,
//         c: 1
//     },
//     "POS_32": {
//         s: 2,
//         r: 1,
//         c: 2
//     },
//     "POS_33": {
//         s: 2,
//         r: 1,
//         c: 3
//     }
// };


var rfid_sensor_positions = {
    "RFID11": {
        s: 0,
        r: 0,
        c: 0
    },
    "RFID12": {
        s: 0,
        r: 0,
        c: 1
    },
    "RFID13": {
        s: 0,
        r: 0,
        c: 2
    },
    "RFID14": {
        s: 0,
        r: 0,
        c: 3
    },
    "RFID15": {
        s: 0,
        r: 1,
        c: 0
    },
    "RFID16": {
        s: 0,
        r: 1,
        c: 1
    },
    "RFID17": {
        s: 0,
        r: 1,
        c: 2
    },
    "RFID18": {
        s: 0,
        r: 1,
        c: 3
    },

    "RFID19": {
        s: 1,
        r: 0,
        c: 0
    },
    "RFID20": {
        s: 1,
        r: 0,
        c: 1
    },
    "RFID21": {
        s: 1,
        r: 0,
        c: 2
    },
    "RFID22": {
        s: 1,
        r: 0,
        c: 3
    },
    "RFID23": {
        s: 1,
        r: 1,
        c: 0
    },
    "RFID24": {
        s: 1,
        r: 1,
        c: 1
    },
    "RFID25": {
        s: 1,
        r: 1,
        c: 2
    },
    "RFID26": {
        s: 1,
        r: 1,
        c: 3
    },

    "RFID27": {
        s: 2,
        r: 0,
        c: 0
    },
    "RFID28": {
        s: 2,
        r: 0,
        c: 3
    },
    "RFID29": {
        s: 2,
        r: 1,
        c: 0
    },
    "RFID30": {
        s: 2,
        r: 1,
        c: 3
    }
};
